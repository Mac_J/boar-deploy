package com.boarsoft.boar.deploy.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.boarsoft.agent.AgentService;
import com.boarsoft.boar.BaseConfig;
import com.boarsoft.boar.deploy.bean.BuildInstParam;
import com.boarsoft.flow.core.bean.JudgeHandler;
import com.boarsoft.rpc.core.RpcContext;

/**
 * 检查应用或目标文件是否已存在
 * 
 * @author Mac_J
 *
 */
public class FileExistsJH implements JudgeHandler {
	private static final Logger log = LoggerFactory.getLogger(FileExistsJH.class);

	@Autowired
	@Lazy
	@Qualifier("agentX")
	private AgentService agentX;

	@Override
	public boolean judge(Object data, Throwable throwable) {
		BuildInstParam p = (BuildInstParam) data;
		String path = p.getTargetPath();
		String axa = new StringBuilder().append(p.getIp()).append(":")//
				.append(BaseConfig.AGENTX_PORT).toString();
		log.info("Ask {} check if file path {} exists", axa, path);
		RpcContext.specify2(axa);
		try {
			return agentX.exists(path);
		} finally {
			RpcContext.specify2(null);
		}
	}
}