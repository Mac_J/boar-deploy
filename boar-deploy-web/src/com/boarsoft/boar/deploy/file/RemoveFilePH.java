package com.boarsoft.boar.deploy.file;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;

import com.boarsoft.agent.AgentService;
import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.BaseConfig;
import com.boarsoft.boar.deploy.bean.DeployAppInstParam;
import com.boarsoft.common.Util;
import com.boarsoft.flow.core.bean.ProcessHandler;
import com.boarsoft.rpc.core.RpcContext;

/**
 * 此Handler会调用位于目标服务器的agentX，备份指定的目录或文件<br>
 * 
 * @author Mac_J
 *
 */
public class RemoveFilePH implements ProcessHandler {
	private static Logger log = LoggerFactory.getLogger(RemoveFilePH.class);

	@Autowired
	@Lazy(value = true)
	@Qualifier("agentX")
	protected AgentService agentX;

	protected String shell;

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		DeployAppInstParam p = (DeployAppInstParam) data;
		String dp = p.getTargetPath();
		if (Util.strIsEmpty(dp)) {
			throw new IllegalArgumentException("Deploy path could not be empty");
		}
		String cmd = shell;
		if (Util.strIsEmpty(cmd)) {
			cmd = p.getApp().getUndeployCode();
		}
		String axa = new StringBuilder().append(p.getIp()).append(":")//
				.append(BaseConfig.AGENTX_PORT).toString();
		log.info("Ask {} delete app instance {}, path = {}", axa, p.getAddr(), dp);
		RpcContext.specify2(axa);
		try {
			ReplyInfo<Object> ro = null;
			if (Util.strIsEmpty(cmd)) {
				ro = agentX.delete(dp);
			} else {
				ro = agentX.executeShell(cmd, new String[] { dp });
			}
			if (ro.isSuccess()) {
				log.info("AgentX {} delete app instance {} successfully", //
						axa, p.getAddr());
				return data;
			}
			throw new Exception(new StringBuilder("AgentX ").append(axa)//
					.append(" remove app instance ").append(p.getAddr())//
					.append(" failed, reason: ")//
					.append(ro.getData()).toString());
		} finally {
			RpcContext.specify2(null);
		}
	}

	public String getShell() {
		return shell;
	}

	public void setShell(String shell) {
		this.shell = shell;
	}
}