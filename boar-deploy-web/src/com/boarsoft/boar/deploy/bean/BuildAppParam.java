package com.boarsoft.boar.deploy.bean;

import java.io.Serializable;

import com.boarsoft.boar.deploy.entity.AppDeployInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;

/**
 * 打包或部署应用时使用此DTO
 * 
 * @author Mac_J
 *
 */
public class BuildAppParam extends BuildItemParam implements Serializable {
	private static final long serialVersionUID = -5332200251459464437L;

	protected AppDeployInfo app;
	
	public BuildAppParam(BuildRequest p, BuildPlanItem a) {
		super(p, a);
	}

	public String getAppId() {
		return app.getId();
	}

	public AppDeployInfo getApp() {
		return app;
	}

	public void setApp(AppDeployInfo app) {
		this.app = app;
	}
}