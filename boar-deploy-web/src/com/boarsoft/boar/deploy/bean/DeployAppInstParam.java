package com.boarsoft.boar.deploy.bean;

import java.io.Serializable;

import com.boarsoft.boar.deploy.entity.AppDeployInfo;

/**
 * 部署某个应用的某个实例时使用此DTO
 * 
 * @author Mac_J
 *
 */
public class DeployAppInstParam extends BuildInstParam implements Serializable {
	private static final long serialVersionUID = -5332200251459464437L;

	protected AppDeployInfo app;
	protected boolean cover = true;

//	public DeployAppInstParam(BuildItemParam p, BuildPlanDetail s) {
//		super(p, s);
//	}

	public AppDeployInfo getApp() {
		return app;
	}

	public void setApp(AppDeployInfo app) {
		this.app = app;
	}

	public boolean isCover() {
		return cover;
	}

	public void setCover(boolean cover) {
		this.cover = cover;
	}
}