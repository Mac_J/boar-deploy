package com.boarsoft.boar.deploy.bean;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.boarsoft.boar.deploy.entity.BuildPlanDetail;

public class BuildInstParam implements Serializable {
	private static final long serialVersionUID = 5364530758162777312L;

	protected String env;
	protected String taskId;

	protected String code;
	protected String version;
	protected String packPath;
	protected String packServer;

	protected String addr;
	protected String ip;
	protected int port;
	protected String targetPath;

	/** 其它可选参数 */
	protected Map<String, Object> data = new HashMap<>();

	/**
	 * 允许绕开BuildItemParam和BuildPlanDetail直接构建
	 */
	public BuildInstParam() {
	}

	public BuildInstParam(BuildItemParam p, BuildPlanDetail s) {
		env = p.getEnv();
		taskId = p.getTaskId();
		//
		packServer = p.getPackServer();
		code = p.getCode();
		version = p.getVersion();
		packPath = p.getPackPath();
		//
		addr = s.getAddr();
		ip = s.getIp();
		port = s.getTargetPort();
		targetPath = s.getTargetPath();
	}

	public Object get(String key) {
		return data.get(key);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> c, String key) {
		return (T) data.get(key);
	}

	/**
	 * 远程目标实例的地址，而不是远程AgentX的地址
	 * 
	 * @return
	 */
	public String getAddr() {
		return addr;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public String getIp() {
		return ip;
	}

	public int getPort() {
		return port;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getPackServer() {
		return packServer;
	}

	public void setPackServer(String packServer) {
		this.packServer = packServer;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}
}