package com.boarsoft.boar.deploy.bean;

import java.io.Serializable;
import java.util.List;

import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;

public class BuildItemParam implements Serializable {
	private static final long serialVersionUID = 5364530758162777312L;

	protected String env;
	protected String taskId;

	protected String planId;
	protected String targetId;
	protected String code; // target code
	protected String version; // target version
	protected String packPath;
	protected String packServer;

	protected List<BuildPlanDetail> details;

	public BuildItemParam(BuildRequest p, BuildPlanItem a) {
		//
		env = p.getEnv();
		taskId = p.getId();
		//
		planId = a.getPlanId();
		targetId = a.getTargetId();
		code = a.getCode();
		version = a.getVer();
		packPath = a.getPackPath();
		// packServer取决于部署的环境，并非来自BuildPlanItem
	}

	public List<BuildPlanDetail> getDetails() {
		return details;
	}

	public void setDetails(List<BuildPlanDetail> details) {
		this.details = details;
	}

	public String getPlanId() {
		return planId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

	public String getPackServer() {
		return packServer;
	}

	public void setPackServer(String packServer) {
		this.packServer = packServer;
	}

	// public String getTargetId() {
	// return item.getTargetId();
	// }
}