package com.boarsoft.boar.deploy.app;

import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.flow.core.bean.JudgeHandler;

/**
 * 为指定的应用实例查找备用节点
 * 
 * @author Mac_J
 *
 */
public class FoundBackupJH implements JudgeHandler {
	public boolean judge(Object data, Throwable throwable) {
		BuildRequest br = (BuildRequest) data;
		return br.exists("inst2");
	}
}