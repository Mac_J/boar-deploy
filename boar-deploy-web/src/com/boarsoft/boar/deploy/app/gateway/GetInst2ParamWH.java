package com.boarsoft.boar.deploy.app.gateway;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.boarsoft.boar.app.AppBiz;
import com.boarsoft.boar.app.AppInstBiz;
import com.boarsoft.boar.deploy.bean.DeployAppInstParam;
import com.boarsoft.boar.deploy.entity.AppDeployInfo;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.boar.entity.AppInst;
import com.boarsoft.boar.entity.ProjInfo;
import com.boarsoft.boar.proj.ProjBiz;
import com.boarsoft.flow.core.bean.WrapHandler;

/**
 * BuildRequest to BuildInstParam for prometheusMasterSlave.xml
 * 
 * @author Mac_J
 *
 */
public class GetInst2ParamWH implements WrapHandler {
	private static final Logger log = LoggerFactory.getLogger(GetInst2ParamWH.class);

	@Autowired
	protected AppBiz appBiz;
	@Autowired
	protected AppInstBiz appInstBiz;
	@Autowired
	protected ProjBiz projBiz;

	@Override
	public Object checkIn(String entry, Object currData) {
		BuildRequest br = (BuildRequest) currData;
		AppInst ai = (AppInst) br.get("inst2");
		log.info("Checkin before deploy app inst {}, taskId = {}", ai, br.getId());
		AppDeployInfo app = (AppDeployInfo) appBiz.get(ai.getAppId());
		ProjInfo proj = projBiz.get(app.getProjId());
		//
		DeployAppInstParam x = new DeployAppInstParam();
		x.setAddr(ai.getAddr());
		x.setApp(app); //
		x.setEnv(ai.getEnv());
		x.setIp(ai.getIp());
		x.setPackPath(app.getPackPath2());
		x.setPackServer(proj.getPackingAddr());
		x.setPort(ai.getPort());
		x.setTargetPath(ai.getDeployPath());
		x.setTaskId(br.getId());
		x.setVersion(app.getVer());
		x.setCover(false); // 如果有部署就直接拉起，不进行部署
		x.setData(br.getData());
		return x;
	}

	public Object checkOut(Object nodeData, Object currData) {
		return currData;
	}
}