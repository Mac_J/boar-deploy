package com.boarsoft.boar.deploy.app;

import org.springframework.beans.factory.annotation.Autowired;

import com.boarsoft.boar.app.AppInstBiz;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.boar.entity.AppInst;
import com.boarsoft.boar.entity.ObjLabel;
import com.boarsoft.boar.label.ObjLabelBiz;
import com.boarsoft.common.Util;
import com.boarsoft.flow.core.bean.ProcessHandler;

/**
 * 为指定的应用实例查找备用节点
 * 
 * @author Mac_J
 *
 */
public class FindBackupPH implements ProcessHandler {
	@Autowired
	protected ObjLabelBiz objLabelBiz;
	@Autowired
	protected AppInstBiz appInstBiz;

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		BuildRequest br = (BuildRequest) data;
		AppInst ai = (AppInst) br.get("inst1");// 原实例（宕机的实例）
		// 查找backup标签值为目标应用
		String v = ai.getKey();
		if (Util.strIsEmpty(v)) {
			v = ai.getAddr();
		}
		ObjLabel ol = objLabelBiz.find(ObjLabel.TYPE_APP_INST, "backup", v);
		if (ol == null) {
			return data;
		}
		br.put("inst2", appInstBiz.get(ol.getObjId()));
		return data;
	}
}