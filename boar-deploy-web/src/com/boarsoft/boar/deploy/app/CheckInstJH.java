package com.boarsoft.boar.deploy.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.boarsoft.boar.deploy.bean.DeployAppInstParam;
import com.boarsoft.boar.deploy.entity.AppDeployInfo;
import com.boarsoft.flow.core.bean.JudgeHandler;
import com.boarsoft.soagov.health.HealthChecker;

/**
 * 检查应用是否健康
 * 
 * @author Mac_J
 *
 */
public class CheckInstJH implements JudgeHandler {
	private static final Logger log = LoggerFactory.getLogger(CheckInstJH.class);

	@Autowired
	protected ApplicationContext appCtx;

	@Override
	public boolean judge(Object data, Throwable throwable) {
		DeployAppInstParam p = (DeployAppInstParam) data;
		AppDeployInfo app = p.getApp();
		String h = app.getHandler();
		HealthChecker checker = appCtx.getBean(HealthChecker.class, h);
		log.info("Check healthy of app inst {}/{} with {}", app.getId(), p.getAddr(), h);
		return checker.check(p.getAddr());
	}
}