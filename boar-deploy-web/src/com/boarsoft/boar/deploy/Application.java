package com.boarsoft.boar.deploy;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.aop.AopAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.TransactionAutoConfiguration;
import org.springframework.boot.autoconfigure.transaction.jta.JtaAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;

import com.boarsoft.rpc.core.RpcCore;

@ImportResource(locations = { "classpath:spring/context.xml" })
@Configuration
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
		JtaAutoConfiguration.class, AopAutoConfiguration.class, TransactionAutoConfiguration.class })
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class Application {
	public Application(RpcCore rpcCore) {
		// Nothing to do
	}

	public static void main(String[] args) throws NumberFormatException, IOException {
		SpringApplication.run(Application.class, args);
	}
}