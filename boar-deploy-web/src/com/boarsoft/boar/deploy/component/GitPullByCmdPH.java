package com.boarsoft.boar.deploy.component;

import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.common.util.StringUtil;
import com.boarsoft.flow.core.bean.ProcessHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

/**
*
* 	windows下的git拉取组件，需要提前打通git权限
*   输入：BuildRequest
*   输出：BuildRequest(补充了拉取工程的目录)
* */
public class GitPullByCmdPH implements ProcessHandler {

	private String gitUser;

	private String gitPassword;

	private String localPath;

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		BuildPlanItem bp=((BuildRequest)data).getCurPlanItem();
		if(StringUtil.isBlank(bp.getSourcePath())){
			throw new RuntimeException("sourcePath is null");
		}
		String clonePath=localPath+"/"+bp.getCode()+"-"+System.currentTimeMillis();
		String projPath=clonePath + bp.getSourcePath().substring(bp.getSourcePath().lastIndexOf("/")).replace(".git","");
		if(!new File(clonePath).exists()){
			new File(clonePath).mkdirs();
		}
		String cloneCmd="cmd /c git clone "+bp.getSourcePath();
		Process process = Runtime.getRuntime().exec(cloneCmd,null,new File(clonePath));
		try (BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
			String line;
			while ((line = outputReader.readLine()) != null) {
				System.out.println(line);
			}
		}

		try (BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
			String line;
			while ((line = errorReader.readLine()) != null) {
				System.err.println(line);
			}
		}
		int exitCode = process.waitFor();
		if(exitCode!=0){
			throw new RuntimeException("There is an error happened when try to git clone");
		}
		bp.setPackPath(projPath);
		//分支为空或master时 不需切换分支
		if(bp.getBranch()!=null&&!bp.getBranch().equals("master")){
			checkoutBranch(bp);
		}
		return data;
	}
	private void checkoutBranch(BuildPlanItem bp) throws Throwable {
		String checkoutCmd = "git checkout -b "+ bp.getBranch() +" origin/"+bp.getBranch();
		Process process = Runtime.getRuntime().exec(checkoutCmd,null,new File(bp.getPackPath()));
		try (BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
			String line;
			while ((line = outputReader.readLine()) != null) {
				System.out.println(line);
			}
		}

		try (BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()))) {
			String line;
			while ((line = errorReader.readLine()) != null) {
				System.err.println(line);
			}
		}
		int exitCode = process.waitFor();
		if(exitCode!=0){
			throw new RuntimeException("There is an error happened when try to checkout branch");
		}
	}
	
	public String getGitUser() {
		return gitUser;
	}

	public void setGitUser(String gitUser) {
		this.gitUser = gitUser;
	}

	public String getGitPassword() {
		return gitPassword;
	}

	public void setGitPassword(String gitPassword) {
		this.gitPassword = gitPassword;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}
}
