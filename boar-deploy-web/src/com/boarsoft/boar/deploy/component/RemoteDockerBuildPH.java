package com.boarsoft.boar.deploy.component;

import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.flow.core.bean.ProcessHandler;
import com.jcraft.jsch.*;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class RemoteDockerBuildPH implements ProcessHandler {

	private String remoteAddress;

	private String remoteUser;

	private String remotePasswd;

	private String dockerUrl;

	private String dockerRepo;

	private String dockerUser;

	private String dockerPasswd;

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		BuildPlanItem bp=((BuildRequest)data).getCurPlanItem();
		String remotePath=bp.getRemotePath();
		//镜像版本号默认1.0.0
		String imageName=dockerUrl+"/"+dockerRepo+"/"+bp.getCode()+":v1.0.0";
		String tagName=bp.getCode()+":v1.0.0";
		//上传ZIP包
		JSch jsch = new JSch();
		Session session = null;
		ChannelSftp sftpChannel = null;

		try {
			//建立ssh连接
			session = jsch.getSession(remoteUser, remoteAddress, 22);
			session.setPassword(remotePasswd);
			session.setConfig("StrictHostKeyChecking", "no");
			session.connect();
			Channel channel = session.openChannel("sftp");
			channel.connect();

			//远程执行docker build
			ChannelExec channelExec = (ChannelExec) session.openChannel("exec");
			String cmd=	"cd " + remotePath + ";" + "docker build -t " + imageName + " .;" +
					"docker tag " + imageName + " " + tagName + ";" +
					"docker login -u " + dockerUser + " -p " + dockerPasswd + " " + dockerUrl +";"+
					"docker push " + imageName + ";";
			channelExec.setCommand(cmd);
			channelExec.setErrStream(System.err);
			channelExec.getInputStream();
			channelExec.connect();

			// Read the output from the command
			InputStream errStream = channelExec.getErrStream();
			InputStream in = channelExec.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			String line;
			while ((line = reader.readLine()) != null) {
				System.out.println(line);
			}

			reader = new BufferedReader(new InputStreamReader(errStream));
			while ((line = reader.readLine()) != null) {
				System.err.println(line);
			}

			channelExec.disconnect();
			session.disconnect();

		} catch (JSchException e) {
			e.printStackTrace();
		} finally {
			if (sftpChannel != null) {
				sftpChannel.exit();
			}
			if (session != null) {
				session.disconnect();
			}
		}
		return data;
	}

	public String getRemoteAddress() {
		return remoteAddress;
	}

	public void setRemoteAddress(String remoteAddress) {
		this.remoteAddress = remoteAddress;
	}

	public String getRemoteUser() {
		return remoteUser;
	}

	public void setRemoteUser(String remoteUser) {
		this.remoteUser = remoteUser;
	}

	public String getRemotePasswd() {
		return remotePasswd;
	}

	public void setRemotePasswd(String remotePasswd) {
		this.remotePasswd = remotePasswd;
	}

	public String getDockerUrl() {
		return dockerUrl;
	}

	public void setDockerUrl(String dockerUrl) {
		this.dockerUrl = dockerUrl;
	}

	public String getDockerRepo() {
		return dockerRepo;
	}

	public void setDockerRepo(String dockerRepo) {
		this.dockerRepo = dockerRepo;
	}

	public String getDockerUser() {
		return dockerUser;
	}

	public void setDockerUser(String dockerUser) {
		this.dockerUser = dockerUser;
	}

	public String getDockerPasswd() {
		return dockerPasswd;
	}

	public void setDockerPasswd(String dockerPasswd) {
		this.dockerPasswd = dockerPasswd;
	}
}
