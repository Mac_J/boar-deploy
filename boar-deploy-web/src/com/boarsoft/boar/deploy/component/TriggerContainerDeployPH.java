package com.boarsoft.boar.deploy.component;

import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.common.util.HttpClientUtil;
import com.boarsoft.flow.core.bean.ProcessHandler;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TriggerContainerDeployPH implements ProcessHandler {

	private String deployUrl;

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		BuildPlanItem bp=((BuildRequest)data).getCurPlanItem();
		if(!new File(bp.getPackPath()+"/deploy.json").exists()){
			throw new RuntimeException("cannot found deploy.json");
		}
		String content = new String(Files.readAllBytes(Paths.get(bp.getPackPath()+"/deploy.json")));
		HttpClientUtil.sendJsonPost(deployUrl,"UTF-8",content,null);
		return data;
	}

	public String getDeployUrl() {
		return deployUrl;
	}

	public void setDeployUrl(String deployUrl) {
		this.deployUrl = deployUrl;
	}
}
