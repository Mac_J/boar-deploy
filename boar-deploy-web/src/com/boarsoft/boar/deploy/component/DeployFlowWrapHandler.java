package com.boarsoft.boar.deploy.component;

import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.flow.core.bean.WrapHandler;

public class DeployFlowWrapHandler implements WrapHandler {

	private String appId;

	@Override public Object checkIn(String entry, Object currData) {
		BuildRequest buildRequest=(BuildRequest)currData;
		buildRequest.setCurrentAppId(appId);
		buildRequest.setCurPlanItem(buildRequest.getByAppId(appId));
		return buildRequest;
	}

	@Override public Object checkOut(Object nodeData, Object currData) {
		return nodeData;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
}
