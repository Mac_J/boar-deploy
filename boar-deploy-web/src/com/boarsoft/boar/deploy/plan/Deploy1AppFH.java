package com.boarsoft.boar.deploy.plan;

import com.boarsoft.boar.deploy.bean.BuildAppParam;
import com.boarsoft.boar.deploy.bean.BuildInstParam;
import com.boarsoft.boar.deploy.bean.BuildItemParam;
import com.boarsoft.boar.deploy.bean.DeployAppInstParam;
import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.flow.core.bean.ForkHandler;

/**
 * 将当前部署项的所有实例按指定线程数拆成多个实例列表，每个线程负责串行部署对应列表中的实例<br>
 * 
 * @author Mac_J
 *
 */
public class Deploy1AppFH extends Build1ItemFH implements ForkHandler {

	@Override
	protected BuildInstParam convert(BuildItemParam p, BuildPlanDetail s) {
		BuildAppParam o = (BuildAppParam) p;
		DeployAppInstParam x = new DeployAppInstParam();
		x.setApp(o.getApp());
		x.setPackServer(o.getPackServer());
		x.setTaskId(o.getTaskId());
		return x;
	}
}