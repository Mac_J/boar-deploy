package com.boarsoft.boar.deploy.plan;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.boarsoft.boar.app.AppBiz;
import com.boarsoft.boar.deploy.bean.BuildAppParam;
import com.boarsoft.boar.deploy.bean.BuildItemParam;
import com.boarsoft.boar.deploy.entity.AppDeployInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.boar.entity.ProjInfo;
import com.boarsoft.boar.proj.ProjBiz;
import com.boarsoft.common.Util;
import com.boarsoft.flow.core.bean.WrapHandler;

/**
 * 从所有部署项中根据key筛选出指定的部署项信息，转换成单项部署参数DeployAppParam<br>
 * 为应用部署流程准备数据
 * 
 * @author Mac_J
 *
 */
public class Deploy1AppWH extends Build1ItemWH implements WrapHandler {
	@Autowired
	protected AppBiz appBiz;
	@Autowired
	protected ProjBiz projBiz;
	@Autowired
	protected PlanDetailBiz itemInstBiz;

	@Override
	protected BuildItemParam convert(BuildRequest r, BuildPlanItem o) {
		BuildAppParam p = new BuildAppParam(r, o);
		String appId = o.getTargetId();
		// 取应用信息
		AppDeployInfo app = (AppDeployInfo) appBiz.get(appId);
		p.setApp(app);
		// 取打包服务器地址
		ProjInfo proj = projBiz.get(app.getProjId());
		if (Util.strIsEmpty(proj.getPackingAddr())) {
			throw new IllegalStateException(new StringBuilder()//
					.append("Packing addr of project ").append(app.getProjId())//
					.append(" is empty").toString());
		}
		p.setPackServer(proj.getPackingAddr());
		// 查询当前部署项的目标服务器列表
		List<BuildPlanDetail> sLt = itemInstBiz.list(//
				o.getPlanId(), appId, r.getEnv());
		p.setDetails(sLt);
		return p;
	}
}