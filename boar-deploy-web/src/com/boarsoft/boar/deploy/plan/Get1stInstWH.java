package com.boarsoft.boar.deploy.plan;

import java.util.List;

import com.boarsoft.boar.deploy.bean.BuildInstParam;
import com.boarsoft.flow.core.bean.WrapHandler;

/**
 * 从列表中取出一个BuildInstParam<br>
 * 
 * @author Mac_J
 *
 */
public class Get1stInstWH implements WrapHandler {
	@SuppressWarnings("unchecked")
	@Override
	public Object checkIn(String entry, Object currData) {
		return ((List<BuildInstParam>) currData).remove(0);
	}

	@Override
	public Object checkOut(Object nodeData, Object currData) {
		return currData;
	}
}