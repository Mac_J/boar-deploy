package com.boarsoft.boar.deploy.plan;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.boar.deploy.bean.BuildItemParam;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;
import com.boarsoft.flow.core.bean.WrapHandler;

/**
 * 从所有部署项中根据key筛选出指定的部署项信息，转换成单项部署参数BuildItemParam<br>
 * 目前只有Deploy1AppWH一个实现
 * 
 * @author Mac_J
 *
 */
public abstract class Build1ItemWH implements WrapHandler {
	private static Logger log = LoggerFactory.getLogger(Build1ItemWH.class);

	/** ${code}-${version} */
	protected String key;

	@Override
	public Object checkIn(String entry, Object currData) {
		BuildRequest p = (BuildRequest) currData;
		for (BuildPlanItem a : p.getItems()) {
			String k = new StringBuilder().append(a.getCode())//
					.append("-").append(a.getVer()).toString();
			if (k.equals(key)) {
				log.info("Found build item with key = {}", key);
				return this.convert(p, a);
			}
		}
		log.error("Can not find build item with key = {}", key);
		return null;
	}

	/**
	 * 由子类实现
	 * 
	 * @param p
	 * @param a
	 * @return
	 */
	protected BuildItemParam convert(BuildRequest p, BuildPlanItem a) {
		return new BuildItemParam(p, a);
	}

	@Override
	public Object checkOut(Object nodeData, Object currData) {
		return currData;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}