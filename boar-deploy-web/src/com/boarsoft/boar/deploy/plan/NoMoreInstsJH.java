package com.boarsoft.boar.deploy.plan;

import java.util.List;

import com.boarsoft.boar.deploy.bean.BuildInstParam;
import com.boarsoft.flow.core.bean.JudgeHandler;

/**
 * 判断BuildInstParam列表中是否还有剩余的BuildInstParam（是否已全部完成或分发）
 * 
 * @author Mac_J
 *
 */
public class NoMoreInstsJH implements JudgeHandler {

	@SuppressWarnings("unchecked")
	@Override
	public boolean judge(Object data, Throwable throwable) {
		return data == null || ((List<BuildInstParam>) data).isEmpty();
	}
}