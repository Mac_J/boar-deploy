package com.boarsoft.boar.deploy.vm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.flow.core.bean.ProcessHandler;

public class ApplyFailedPH implements ProcessHandler {
	private static final Logger log = LoggerFactory.getLogger(Apply4InstPH.class);

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		// TODO
		log.info("Apply resource for app inst {} failed", data);
		return data;
	}
}