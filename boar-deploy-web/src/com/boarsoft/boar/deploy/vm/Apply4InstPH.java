package com.boarsoft.boar.deploy.vm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.flow.core.bean.ProcessHandler;

/**
 * 为应用实例申请虚拟机资源
 * 
 * @author Mac_J
 *
 */
public class Apply4InstPH implements ProcessHandler {
	private static final Logger log = LoggerFactory.getLogger(Apply4InstPH.class);

	@Override
	public Object process(String entry, Object data, Throwable throwable) throws Throwable {
		// TODO
		log.info("Apply resource for app inst {}", data);
		return data;
	}
}