#!/bin/bash

#echo $1

print_input_err() 
{
	echo "输入参数错误，请重新输入。"
	echo "功能: 导入部署计划"
	echo "参数1（必要）: 自动化部署系统的地址（域名或IP）"
	echo "参数2（必要）: 部署计划的相对路径"
	return 0
}

if [[ $1 == '' ]] || [[ $2 == '' ]]; then
	print_input_err
	exit 1
fi


curl -s -o import_log.txt -D boar-web-ck.txt "http://${1}/deploy/sys/user/login.do?code=admin&password=e10adc3949ba59abbe56e057f20f883e"

echo "开始导入部署计划："
echo `curl -s -b boar-web-ck.txt "http://${1}/deploy/plan/import.do?importRelPath=${2}"`
