#!/bin/bash

. ./env.conf

echo "##########boar_deploy_web.sh start##########"

sh stop.sh
#is_suc "exec stop.sh"
ret=$?
if [[ "$ret" != "0" ]] && [[ "$ret" != "2" ]]; then
        echo "[ERROR]: exec stop.sh failed"
fi

sh update.sh
is_suc "exec update.sh"

sh start.sh
is_suc "exec start.sh"


echo "##########boar_deploy_web.sh SUC##########"
exit 0