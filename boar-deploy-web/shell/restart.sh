#!/bin/bash

. ./env.conf

echo "##########start boar-deploy-web_restart.sh##########"

sh stop.sh
#is_suc "exec stop.sh"
ret=$?
if [[ "$ret" != "0" ]] && [[ "$ret" != "2" ]]; then
        echo "[ERROR]: exec stop.sh failed"
fi

sh start.sh
is_suc "exec start.sh"

echo "##########boar-deploy-restart.sh SUC##########"



exit 0