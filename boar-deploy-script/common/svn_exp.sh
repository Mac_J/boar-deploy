#!/bin/bash

#"功能: svn文件导出"
#svn_url 检出路径: 
#local_src_path 本地路径: 
#svn_revision 检出版本号: 传入null按最新版本号检出
#例: sh svn_exp.sh "svn_url=https://20.3.6.172/svn/boarsoft/boar/trunk/boar-deploy-script&local_src_path=/home/nlzx/test/boar-deploy-script&svn_revision=630"
#
#注: 1. 检查local_src_path是否存在, 不存在则创建该目录
#    2. 检查local_src_path目录下是否存在.svn子目录, 若不存在判定
#       local_src_path为首次下载使用svn checkout, 若存在则使用svn update
#    3. 若local_src_path目录下不存在.svn子目录且local_src_path目录下有与
#       要下载的目录同名子目录, svn checkout会失败, 请手工清理local_src_path

print_input_err() 
{
	echo "功能: svn文件导出"
	echo "svn_url, 检出路径"
	echo "local_src_path, 本地路径"
	echo "svn_vers, 检出版本号"
	echo ""
	
	return 0
}

is_suc()
{
	if [ $? -eq 0 ]; then
		echo "[INFO]: command [$1] is success"
		return 0
	else
		echo ""
		echo "[ERROR]: command [$1] is failed!!!"
		exit 1
	fi
}

#$1 - 入参,检出路径
#$2 - 入参,本地路径
#$3 - 入参,svn账号
#$4 - 入参,svn密码
#$5 - 入参,svn revision
#功能: 从svn下载源码
export_src()
{
	local check_out_path
	local local_src
	local command
	local svn_flag #0-svn子目录; else-非svn子目录
	local ret

	local_src=$2
	if [ ! -d "${local_src}" ]; then
		mkdir -p ${local_src}
	fi

	cd ${local_src}
	is_suc "cd [${local_src}]"

    #rm -rf ${local_src}/*
    #is_suc "rm -rf ${local_src}/*"
    
    command="svn"
    #判断本地目录是否为svn检出目录
    ret=`ls -aF |grep '/$' |grep ".svn" 2>&1`
    svn_flag=$?
    if [[ "$svn_flag" == "0" ]]; then
    	echo "[DEBUG]: CURLINE[$LINENO], [${local_src}] is svn subdirectory"
    	command=`echo "$command update"`
    	
    	if [[ "$5" != "" ]] && [[ "$5" != "${NULL_VAL}" ]]; then
    		command=`echo "${command} -r $5"`
    	fi
    else
    	echo "DEBUG]: CURLINE[$LINENO], ret[$ret]"
    	command=`echo "$command checkout $1 ."`
    fi
    command=`echo "$command --username=${3} --password=${4} --force --no-auth-cache"`
    echo "DEBUG]: CURLINE[$LINENO], command[$command]"

	#if [[ "$5" == "" ]] || [[ "$5" == "${NULL_VAL}" ]]; then
	#	check_out_path=$1
	#else
	#	check_out_path="-r $5 ${1}"
	#fi
    
	#svn --username=${3} --password=${4} export ${check_out_path} ${local_src} --force --no-auth-cache
	#is_suc "svn export [${check_out_path}] [${local_src}] --force --no-auth-cache"
	$command
	is_suc "CURLINE[$LINENO], [$command]"
    
	return 0
}

env_path=`dirname $0`
is_suc "CURLINE[$LINENO], dirname $0"

. ${env_path}/env.conf
is_suc "CURLINE[$LINENO], . ${env_path}/env.conf"

get_para "$*" ${CMP_SEP_PAR} ${CMP_SEP_VAL}
export_src "${svn_url}" "${local_src_path}" ${SVN_USR} ${SVN_PAS} ${svn_revision}

echo "end svn_exp.sh"
exit 0

