#!/bin/bash

#功能: mvn工程代码编译
#local_src_path 本地代码路径
#project_root_path: 输入null无需project.root.path
#cmp_env 编译环境: 输入null, pom.xml中无profile编译项或使用默认编译项;
#mvn_comm 编译命令: 0-install; 1-deploy; 2-package;
#compile_type 打包方式: assembly类型, 0-libs; 1-full; 2-conf;
#target_path 编译生成包移动至该目录: null不移动
#package_type 生成包类型(移动生成包时使用本项): 0-zip; 1-jar; 2-war; (若target_path使用null, 本项可输入null)
#例: sh mvn_cmp.sh "local_src_path=test/boar-web; project_root_path=; cmp_env=SIT; mvn_comm=0; compile_type=null; target_path=/home/nlzx; package_type=2"

. ./env.conf
#get_para "a|111; b|222; c|333; d=" "; " "|"
#echo "xxx: d[$d]"
#exit 0

print_input_err() 
{
	echo "功能: mvn工程代码编译"
	echo "local_src_path 本地代码路径"
	echo "project_root_path: 输入null无需project.root.path"
	echo "cmp_env 编译环境: 输入null, pom.xml中无profile编译项或使用默认编译项;"
	echo "mvn_comm 编译命令: 0-install; 1-deploy; 2-package"
	echo "compile_type 打包方式: assembly类型, 0-libs; 1-full; 2-conf;"
	echo "target_path 编译生成包移动至该目录: null不移动"
	echo "package_type 生成包类型(移动生成包时使用本项): 0-zip; 1-jar; 2-war; (若target_path使用null, 本项可输入null)"
	echo ""
	
	return 0
}


is_suc()
{
	if [ $? -eq 0 ]; then
		echo "[INFO]: command [$1] is success"
		return 0
	else
		echo ""
		echo "[ERROR]: command [$1] is failed!!!"
		exit 1
	fi
}


#功能: 编译代码
#$1 - 入参,本地代码路径
#$2 - 入参,编译环境
#$3 - 入参,project.root.path
#$4 - 入参,编译包放置路径
#$5 编译命令: 0-install; 1-deploy;
#$6 生成包类型(移动生成包时使用本项): 0-zip; 1-jar; 2-war; (若$6使用null, 本项可输入null)
#$7 打包方式: assembly类型, 0-libs; 1-full; 2-conf;
compile()
{
	local mvn_c=$5
	local pack_t=$6
	local comp_t=$7
	local comman_str
	
	echo "compile: \$1[$1] \$2[$2] \$3[$3] \$4[$4] \$5[$5] \$6[$6] \$7[$7]"

	cd $1
	is_suc "cd $1"

	comman_str="mvn -U clean ${mvn_c} -Dmaven.test.skip=true -Dmaven.test.failture.ignore=true -P${2} -Dproject.root.path=$3 -Dassembly=${comp_t}"
	if [[ "$3" != "${NULL_VAL}" ]];then
		comman_str="${comman_str} -Dproject.root.path=$3"
	fi
	if [[ "$comp_t" != "${NULL_VAL}" ]];then
		comman_str="${comman_str} -Dassembly=${comp_t}"
	fi
	
	echo "start mvn command"
	ret=`${comman_str}`
	if [[ $? -ne 0 ]]; then
		echo "[ERROR]: command [${comman_str}] is failed!!!"
		echo $ret |sed 's/\[/\n\[/g' |awk '/ERROR/ {print}' 
		exit 1
	else
		echo "[INFO]: command [${comman_str}] is success"
	fi

	if [[ "$4" == "" ]] || [[ "$4" == "null" ]]; then
		echo "[INFO]: 不移动生成包"
		return 0
	fi
	if [ ! -d "$4" ]; then
		mkdir -p $4
		is_suc "mkdir -p [$4]"
	fi
    
	find ${1} -name "*.${pack_t}" -exec mv {} ${4} \;
	is_suc "find ${1} -name "*.${pack_t}" -exec mv {} ${4} \;"

	return 0
}

#---------------------------------------------------

echo "start mvn_cmp.sh: [$1]"
get_para "$*" ${CMP_SEP_PAR} ${CMP_SEP_VAL}
#local_src_path=$1
#project_root_path=$2
#cmp_env=$3
#target_path=$6
#mvn_comm=$4
#package_type=$7
#compile_type=$5

if [[ "${mvn_comm}" == "" ]] || [[ "${mvn_comm}" == "${NULL_VAL}" ]]; then
	mvn_comm="${NULL_VAL}"
elif [[ ${mvn_comm} -ge 0 ]] && [[ ${mvn_comm} -lt ${#arr_mvn_c[@]} ]]; then
	mvn_comm=${arr_mvn_c[${mvn_comm}]}
else
	echo "输入[编译命令], [${mvn_comm}]错误"
	print_input_err
	exit 1
fi

if [[ "${package_type}" == "" ]] || [[ "${package_type}" == "${NULL_VAL}" ]]; then
	package_type="${NULL_VAL}"
elif [[ ${package_type} -ge 0 ]] && [[ ${package_type} -lt ${#arr_pack_t[@]} ]]; then
	package_type=${arr_pack_t[${package_type}]}
else
	echo "输入[生成包类型], [${package_type}]错误"
	print_input_err
	exit 1
fi

if [[ "${compile_type}" == "" ]] || [[ "${compile_type}" == "${NULL_VAL}" ]]; then
	compile_type="${NULL_VAL}"
elif [[ ${compile_type} -ge 0 ]] && [[ ${compile_type} -lt ${#arr_comp_t[@]} ]]; then
	compile_type=${arr_comp_t[${compile_type}]}
else
	echo "输入[打包方式], [${compile_type}]错误"
	print_input_err
	exit 1
fi


#local_src_path="${LOCAL_SRC_HOME}/${local_src_path}"
compile ${local_src_path} ${cmp_env} ${project_root_path} ${target_path} ${mvn_comm} ${package_type} ${compile_type}
is_suc "compile ${local_src_path} ${cmp_env} ${project_root_path} ${target_path} ${mvn_comm} ${package_type} ${compile_type}"


echo "end mvn_cmp.sh"
exit 0

