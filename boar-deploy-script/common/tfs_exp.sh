#!/bin/bash

#"功能: tfs文件导出"
#source_server: tfs default collection url <http://10.16.10.97:8080/tfs/defaultcollection>
#check_path: tfs路径 <$/电子银行产品线/能力中心/REL_20170713/ebc-parent>
#tfs_space_name: tfs工作空间 <自动化部署平台使用projId赋值>
#local_src_path 本地路径: 
#tfs_usr: 不输或者输入null使用env.conf中配置
#tfs_pass: 不输或者输入null使用env.conf中配置
#例: sh tfs_exp.sh "source_server=http://10.16.10.97:8080/tfs/defaultcollection&check_path=$/电子银行产品线/能力中心/REL_20170713/ebc-parent&tfs_space_name=tf_workspace_ebbc&local_src_path=/home/nlzx/test/ebc-parent"


#功能: 判断命令返回值;
#$1 - 输出信息
is_suc()
{
	if [ $? -eq 0 ]; then
		echo "[INFO]: command [$1] is success"
		return 0
	else
		echo ""
		echo "[ERROR]: command [$1] is failed!!!"
		exit 1
	fi
}

#$1 - 出参,当前时间(YYYY-MM-DD+HH:MM:SS+nanoseconds)
get_cur_time()
{
	local resultvar__=$1
    #eval $resultvar__=`date +"%Y-%m-%d+%H:%M:%S+%N"`
    eval $resultvar__=`date +"%Y%m%d%H%M%S%N"`
    
	return 0
}

#功能: 获取workspaces相关信息
#$1: 出参, tfs workspaces server信息
#$2: 出参, tfs workspaces 本地信息
#$3: 入参, tfs default collection url 
#$4: 入参, workspaces name <tf_workspace_ebbc>
#$5: 入参, tfs user
#$6: 入参, tfs password
get_workspaces_info ()
{
	local space_info
	local server_info=$1
	local local_info=$2
	local LOGIN=-login

	#tf help workspace
	#tf help workspaces
	space_info=`tf workspaces -format:xml ${4} -collection:${3} ${LOGIN}:${5},${6}`
	is_suc "get tfs workspace info"
	echo "space_info: [${space_info}]"
	
	#local_info=`echo ${space_info} |tr '>' '\n' |awk -F"local-item=| |\"" '/local-item=/ {print $8}'`
	#local_info=`echo ${space_info} |tr ' ' '\n' |awk -F"\"" '/local-item=/ {print $2}'
	if [[ "$local_info" ]]; then
		eval $local_info=`echo ${space_info} |tr ' ' '\n' |awk -F"\"" '/local-item=/ {print $2}'`
	else
		return 1
	fi
	is_suc "get tfs local_info"
	
	if [[ "$server_info" ]]; then
		eval $server_info=`echo ${space_info} |tr ' ' '\n' |awk -F"\"" '/server-item=/ {print $2}'`
	else
		return 1
	fi
	is_suc "get tfs server_info"
	
	return 0
}

#$1 - 入参, tfs workspace
#返回值: 1-该workspace存在
is_exist_workspace ()
{
	space_name=$1
	local space_info
	local ret
	
	space_info=`tf workspaces |grep "$space_name"`
	ret=$?
	if [[ "$space_info" != "" ]] && [[ "$ret" == "0" ]]; then
		return 1
	fi
	
	return 0
}

#$1 - 入参, 检出路径 
#$2 - 入参, 本地路径
#$3 - 入参, tfs账号
#$4 - 入参, tfs密码
#$5 - 入参, tfs workspace
#$6 - 入参, tfs default collection url 
#功能: 从svn下载源码
export_src()
{
	local check_out_path=$1
	local local_src=$2
	local user=$3
	local pass=$4
	local tfs_space=$5
	local tfs_defa_coll=$6
	local server_item
	local local_item
	local LOGIN=-login
	local ret
	
	if [ $# -lt 6 ]; then
		echo "[ERROR]: export_src() input error"
		exit 1
	fi
	
	if [ ! -d "${local_src}" ]; then
		mkdir -p ${local_src}
	fi
		
	cd ${local_src}
	is_suc "cd ${local_src}"
    
    is_exist_workspace ${tfs_space}
    ret=$?
    if [[ "$ret" != "1" ]]; then
    	echo "创建tfs工作空间[${tfs_space}]"
		tf workspace -new ${tfs_space} -collection:${tfs_defa_coll} ${LOGIN}:${user},${pass}
		is_suc "tf workspace -new ${tfs_space}"
	else
		echo "tfs工作空间[${tfs_space}]已存在"
	fi
	
	#获取工作空间${tfs_space}绑定信息
	get_workspaces_info server_item local_item ${tfs_defa_coll} ${tfs_space} ${user} ${pass}
	is_suc "get_workspaces_info"
	echo "[INFO]: current, server_item[${server_item}], local_item[${local_item}]"
	if [[ "$local_item" != "$local_src" ]] || [[ "$server_item" != "$check_out_path" ]]; then
		if [[ "$local_item" != "" ]]; then
			#解绑工作空间当前绑定目录
			tf workfold -unmap ${local_item} ${LOGIN}:${user},${pass}
			is_suc "tf workfold -unmap ${local_item}"
		fi
		
		#本地目录绑定tfs工作空间
		tf workfold -map ${check_out_path} . -workspace:${tfs_space} ${LOGIN}:${user},${pass}
		is_suc "tf workfold -map"
	fi
	
	echo "start get"
	tf get -all . -r ${LOGIN}:${user},${pass}
	ret=$?
	echo "[DEBUG]: tf get all ret[$?]"
	
	#解绑工作空间当前绑定目录
	tf workfold -unmap . ${LOGIN}:${user},${pass}
	is_suc "tf workfold -unmap ."
			
    #删除workspace
	#tf workspace -delete -collection:${tfs_defa_coll} ${tfs_space} ${LOGIN}:${user},${pass}
	#is_suc "tf workspace -delete ${tfs_space}"

	return 0
}


#----------------------------------------start build----------------------------------------
env_path=`dirname $0`
. ${env_path}/env.conf

echo "[DEBUG]: start tfs_exp.sh, receive[$*]"
get_cur_time cur_time
echo "[INFO]: cur_time[${cur_time}]"

get_para "$*" ${CMP_SEP_PAR} ${CMP_SEP_VAL}
is_suc "get_para()"
if [[ "${tfs_usr}" == "" ]] || [[ "${tfs_usr}" == "null" ]]; then
	tfs_usr=${TFS_USR}
fi
if [[ "${tfs_pass}" == "" ]] || [[ "${tfs_pass}" == "null" ]]; then
	tfs_pass=${TFS_PAS}
fi

#tfs_space_name=tf${cur_time}
server_name=`hostname`
tfs_space_name=`echo "${server_name}${tfs_space_name}"`
export_src "${check_path}" "${local_src_path}" "${tfs_usr}" "${tfs_pass}" "${tfs_space_name}" "${source_server}"
is_suc "export_src()"

get_cur_time cur_time
echo "[INFO]: tfs_exp.sh suc, cur_time[${cur_time}]"

exit 0
