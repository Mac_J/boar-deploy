#$1: /home/app/temp/redis6383
#$2: /home/app/redis6383
#$3: port
cp -r $1 $2
docker rm  redis$3
docker create -p 6379:$3 --name redis$3 -v $2:/etc/redis redis redis-server /etc/redis/redis.conf
