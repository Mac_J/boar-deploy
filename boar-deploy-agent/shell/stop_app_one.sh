#!/bin/bash

#功能: 停用agent;
#参数: app_name  - 应用名, 不输入或输入null时使用env.conf中配置APP_NAME; <boar-deploy-agent>
#      app_path  - 应用部署目录, 不输入或输入null时使用env.conf中配置APP_HOME; </home/app/deploy>
#      sbin_path - 应用目录下stop.sh和start.sh所在目录, 不输入或输入null时使用env.conf中配置SBIN_DIR<sbin>

echo "begin stop_app_one.sh"

#加载配置文件
env_path=`dirname $0`
if [[ -f ${env_path}/env.conf ]]; then
    echo "[DEBUG]: CURLINE[$LINENO], . ${env_path}/env.conf"
    . ${env_path}/env.conf
fi

get_para "$*" ${CMP_SEP_PAR} ${CMP_SEP_VAL}
ret=$?
if [[ "$ret" != "0" ]]; then
    echo "[ERROR]: CURLINE[$LINENO], get_para error"
    exit 1
fi

if [[ "$app_name" == "null" ]] || [[ "$app_name" == "" ]]; then
    if [[ "$APP_NAME" == "null" ]] || [[ "$APP_NAME" == "" ]]; then
        echo "[ERROR]: CURLINE[$LINENO], app_name"
        exit 1
    fi
    app_name=${APP_NAME}
    echo "[DEBUG]: CURLINE[$LINENO], app_name not input, [${app_name}]"
fi

if [[ "$app_path" == "null" ]] || [[ "$app_path" == "" ]]; then
    if [[ "$APP_HOME" == "null" ]] || [[ "$APP_HOME" == "" ]]; then
        echo "[ERROR]: CURLINE[$LINENO], app_path"
        exit 1
    fi
    app_path=${APP_HOME}
    echo "[DEBUG]: CURLINE[$LINENO], app_path not input, [$app_path]"
fi

if [[ "$sbin_path" == "null" ]] || [[ "$sbin_path" == "" ]]; then
    if [[ "$SBIN_DIR" == "null" ]] || [[ "$SBIN_DIR" == "" ]]; then
        echo "[ERROR]: CURLINE[$LINENO], sbin_path"
        exit 1
    fi
    sbin_path=${SBIN_DIR}
    echo "[DEBUG]: CURLINE[$LINENO], sbin_path not input, [$sbin_path]"
fi

ret=`ls -F ${app_path} |grep "/$" |grep ${app_name} |wc -l`
is_suc "CURLINE[$LINENO], get dirname"
if [[ "${ret}" != "1" ]]; then
    echo "[ERROR]: CURLINE[$LINENO], app num[$ret]"
    exit 1
fi
dir_name=`ls -F ${app_path} |grep "/$" |grep ${app_name}`
is_suc "CURLINE[$LINENO], get dir name"

if [ ! -d ${app_path}/${dir_name}/${sbin_path} ]; then
    echo "[ERROR]: CURLINE[$LINENO], path[${app_path}/${dir_name}/${sbin_path}] not found"
    exit 1
fi

cd ${app_path}/${dir_name}/${sbin_path}
is_suc "CURLINE[$LINENO], cd ${app_path}/${dir_name}/${sbin_path}"

sh stop.sh
is_suc "CURLINE[$LINENO], sh stop.sh"


echo "stop_app_one.sh suc"

exit 0

