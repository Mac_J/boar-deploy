#!/bin/bash

#"功能: 使用scp拷贝文件至远程服务器"
#"$1, 原文件目录"
#"$2, 原文件名"
#"$3, 执行标识: <远程拷贝至本地暂忽略此标识>
#"              0 - 原文件不压缩拷贝"
#"              1 - 原文件gz压缩拷贝"
#"              2 - 原文件zip压缩拷贝"
#"              3 - 原文件不压缩拷贝(拷贝zip包), 拷贝后解压缩"
#"$4, 目标服务器ip <远程拷贝至本地时为远程服务器ip>"
#"$5, 目标目录"
#"$6, 目标文件名称"
#"$7, 目标服务器用户名 <远程拷贝至本地时为远程服务器用户名>"
#"$8, 拷贝标识: 1 - 远程拷贝至本地; else - 本地拷贝至远程"


print_input_err()
{
	echo "功能: 拷贝文件"
	echo "\$1, 原文件目录"
	echo "\$2, 原文件名"
	echo "\$3, 执行标识: <远程拷贝至本地暂忽略此标识>"
	echo "              0 - 原文件不压缩拷贝"
	echo "              1 - 原文件gz压缩拷贝"
	echo "              2 - 原文件zip压缩拷贝"
	echo "              3 - 原文件不压缩拷贝(拷贝zip包), 拷贝后解压缩"
	echo "\$4, 目标服务器ip <远程拷贝至本地时为远程服务器ip>"
	echo "\$5, 目标目录"
	echo "\$6, 目标文件名称"
	echo "\$7, 目标服务器用户名 <远程拷贝至本地时为远程服务器用户名>"
	echo "\$8, 拷贝标识: 1 - 远程拷贝至本地; else - 本地拷贝至远程"
	echo ""

	return 0
}

if [[ $# -lt 7 ]]; then
	echo "input error , [$#]"
	print_input_err
	exit 1
fi

is_suc()
{
	if [ $? -eq 0 ]; then
		echo "[INFO]: command $1 is success"
		return 0
	else
		echo ""
		echo "[ERROR]: command $1 is failed!!!"
		exit 1
	fi
}


echo "input [$1], [$2], [$3], [$4], [$5], [$6], [$7]"

src_path=$1
src_file=$2
src_path_file=${src_path}/${src_file}
flag=$3
ip_list="$4"
dest_path=$5
dest_file=$6
app_name=$7
cp_flag=$8

#if [[ "${cp_flag}" != "1" ]] && [[ ! -e "${src_path_file}" ]]; then
#	echo "file[$1] not found!"
#	exit 1
#fi


#$1, 目标ip
#$2, 目标目录
#$3, 目标文件名
#$4, 目标服务器用户名
#$5, 原文件名(含路径)
scp_file()
{
	ssh -Tq $4@$1 << !
		if [ ! -d $2 ]; then
			mkdir -p $2
		fi

		exit 0
!

	scp -r $5 $4@$1:$2
	is_suc "scp -r [$5] [$4@$1:$2]"

	return 0
}

#$1, 目标ip
#$2, 目标目录
#$3, zip包名
#$4, 目标服务器用户名
unzip_remote_file ()
{
	ssh -Tq $4@$1 << !
		cd $2
		unzip -o $3

		exit 0
!

	return 0
}


echo "start rcp.sh"
#本地拷贝文件至远程
local2remote()
{
	case "${flag}" in
		"0"|"3")
	#echo "for test: [${ip_list}] [${dest_path}] [${dest_file}] [${app_name}] [${src_path_file}]"
			scp_file ${ip_list} ${dest_path} ${dest_file} ${app_name} ${src_path_file}
			is_suc "scp_file [${ip_list}] [${dest_path}] [${dest_file}] [${app_name}] [${src_path_file}]"
			
			if [[ "${flag}" == "3" ]]; then
				unzip_remote_file ${ip_list} ${dest_path} ${dest_file} ${app_name}
				is_suc "unzip_remote_file [${ip_list}] [${dest_path}] [${dest_file}] [${app_name}]"
			fi
		;;
	
		"1")
			dest_file=${dest_file}.tar.gz
	
			cd ${src_path}
			is_suc "cd [${src_path}]"
	
			tar -zcvf ${dest_file} ${src_file}
			is_suc "tar -zcvf [${dest_file}] [${src_file}]"
	
			scp_file ${ip_list} ${dest_path} ${dest_file} ${app_name} ${dest_file}
			is_suc "scp_file [${ip_list}] [${dest_path}] [${dest_file}] \
				[${app_name}] [${dest_file}]"
	
			rm ${dest_file}
			is_suc "rm ${dest_file}"
		;;
	
		"2")
			dest_file=${dest_file}.zip
	
			cd ${src_path}
			is_suc "cd [${src_path}]"
	
			zip -q -r ${dest_file} ${src_file}
			is_suc "zip -q -r [${dest_file}] [${src_file}]"
	
			scp_file ${ip_list} ${dest_path} ${dest_file} ${app_name} ${dest_file}
			is_suc "scp_file [${ip_list}] [${dest_path}] [${dest_file}] \
				[${app_name}] [${dest_file}]"
	
			rm ${dest_file}
			is_suc "rm ${dest_file}"
			
		;;
		*)
			echo "input error!!!"
			print_input_err
			exit 1
		;;
	esac
}


#远程拷贝文件至本地
remote2local()
{
	if [ ! -d ${dest_path} ]; then
		mkdir -p ${dest_path}
	fi
	
	scp -r ${app_name}@${ip_list}:${src_path_file} ${dest_path}
	is_suc "scp -r [${app_name}@${ip_list}:${src_path_file}] [${dest_path}]"
}

if [[ "${cp_flag}" == "1" ]]; then
	remote2local
	is_suc "remote2local"
else
	local2remote
	is_suc "local2remote"
fi

echo "end rcp.sh"
exit 0

