package com.boarsoft.boar.deploy;

import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boarsoft.common.Util;

public class DeployConfig {
	private final static Logger log = LoggerFactory.getLogger(DeployConfig.class);
	private final static Properties prop = new Properties();

	static {
		try {
			log.info("Load boar-deploy config.properties ...");
			prop.load(DeployConfig.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			log.error("Error on load config.properties.", e);
		}
	}

	public final static String JENKINS_URL = prop.getProperty("JENKINS.URL", "http://localhost:8080");
	public final static String DEPLOY_CALLERBALE_URL = prop.getProperty("DEPLOY_CALLERBALE.URL");
	public final static String COMPILE_CALLERBALE_URL = prop.getProperty("COMPILE_CALLERBALE.URL");
	public final static String EXPORT_CALLERBALE_URL = prop.getProperty("EXPORT_CALLERBALE.URL");
	/*
	 * deployment
	 */
	public final static String DEPLOY_CREATE_URL_VAL = prop.getProperty("DEPLOY_CREATE_URL_VAL");
	public final static String REMOTE_APP_USR = prop.getProperty("REMOTE_APP_USR");
	public final static String REMOTE_APP_UPLOAD = prop.getProperty("REMOTE_APP_UPLOAD");
	public final static String REMOTE_APP_SBIN = prop.getProperty("REMOTE_APP_SBIN");
	public final static String REMOTE_APP_HOME = prop.getProperty("REMOTE_APP_HOME");
	public final static String LOCAL_TARGET_VSN = prop.getProperty("LOCAL_TARGET_VSN");
	/**
	 * 环境
	 */
	public final static String ENVS = prop.getProperty("ENVS");
	// 改成从数据库表获取配置
	// public static String ENVS = null;
	/**
	 * 前端请求的websocket地址
	 */
	public final static String WEBSOCKET_ADDRESS = prop.getProperty("WEBSOCKET_ADDRESS");

	// 拷贝类型：本地拷贝-lcp
	public final static String LOCAL_COPY = prop.getProperty("LOCAL_COPY");
	// 拷贝类型：远程拷贝-rcp
	public final static String REMOTE_COPY = prop.getProperty("REMOTE_COPY");
	// 删除
	public final static String RM = prop.getProperty("RM");
	// 强制删除
	public final static String RM_RF = prop.getProperty("RM_RF");

	// 能力中心应用备份根目录
	public final static String EBBC_APP_BACKUP_PATH = prop.getProperty("EBBC_APP_BACKUP_PATH");
	// 能力中心应用部署根目录
	public final static String EBBC_APP_DEPLOY_PATH = prop.getProperty("EBBC_APP_DEPLOY_PATH");
	/** 打包服务器上AgentC的端口 */
	@Deprecated
	public final static String RPC_COMM_PORT = prop.getProperty("RPC_COMM_PORT");
	// /** 打包服务器上AgentC的端口 */
	// public final static int AGENTC_PORT = getInt("AGENTC_PORT", 9901);
	// 打包服务器IP
	public final static String COMPILE_SERVER_ADDRESS = prop.getProperty("COMPILE_SERVER_ADDRESS");
	// 打包服务器jar包生成根路径
	public final static String COMPILE_SERVER_JAR_ROOT_DIR = prop.getProperty("COMPILE_SERVER_JAR_ROOT_DIR");
	// boar-web应用部署服务器
	public final static String BOAR_WEB_SERVER_ADDRESS = prop.getProperty("BOAR_WEB_SERVER_ADDRESS");
	// boar-web应用服务器部署用户名
	public final static String BOAR_WEB_DEPLOY_USR = prop.getProperty("BOAR_WEB_DEPLOY_USR");
	// boar-web应用服务器zip文件临时存放目录
	public final static String BOAR_WEB_ZIP_TEMP_DIR = prop.getProperty("BOAR_WEB_ZIP_TEMP_DIR");

	@Deprecated
	// BOAR_WEB_SRC_ROOT=/home/nlzx/boar-web/src
	public final static String BOAR_WEB_SRC_ROOT = prop.getProperty("BOAR_WEB_SRC_ROOT");

	// 服务器存放临时文件的目录
	// public final static String SERVER_TEMP_FILE_PATH =
	// prop.getProperty("SERVER_TEMP_FILE_PATH");
	// 打包服务器部署计划导出根目录
	public final static String COMPILE_SRV_EXPORT_ROOT = prop.getProperty("COMPILE_SRV_EXPORT_ROOT");
	// 通用远程免密账户
	public final static String GENERAL_SSH_USER = prop.getProperty("GENERAL_SSH_USER");
	// boar-web应用服务器导入目录
	public final static String BOAR_WEB_SRV_IMPORT_ROOT = prop.getProperty("BOAR_WEB_SRV_IMPORT_ROOT");
	// boar-web服务器meta临时目录
	public final static String BOAR_WEB_SRV_EXPORT_DIR = "export";
	// 部署计划导出文件夹中流程子目录
	public final static String COMPILE_SRV_EXPORT_FLOW = "flows";
	// 导出部署计划meta文件名
	public final static String EXPORT_META_NAME = "META_INFO.txt";

	// -----------------------

	/** 打包服务器上源码目录 */
	public static final String SRC_ROOT = prop.getProperty("SRC_ROOT");

	public static int getInt(String key, int value) {
		return Util.str2int(prop.getProperty(key), value);
	}
}
