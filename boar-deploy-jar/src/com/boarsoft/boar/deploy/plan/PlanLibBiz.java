package com.boarsoft.boar.deploy.plan;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.entity.LibDeployInfo;
import com.boarsoft.common.dao.PagedResult;

public interface PlanLibBiz {
	/**
	 * 
	 * @param projId
	 * @param planId
	 * @param in
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<LibDeployInfo> list(String projId, String planId, boolean in, String key, String orderBy, int pageNo, int pageSize);

	/**
	 * 
	 * @param o
	 */
	ReplyInfo<Object> save(BuildPlanItem o);
}