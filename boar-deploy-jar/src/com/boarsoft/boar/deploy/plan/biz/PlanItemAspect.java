package com.boarsoft.boar.deploy.plan.biz;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.boarsoft.boar.deploy.plan.PlanItemBiz;
import com.boarsoft.boar.server.ServerBiz;

//@Component
//@Aspect
@Deprecated
public class PlanItemAspect {
	protected Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	protected PlanItemBiz buildPlanItemBiz;
	@Autowired
	protected ServerBiz serverBiz;

	@Pointcut("execution(* com.boarsoft.boar.file.biz.FileBizImpl.delete(String))")
	public void deleteConfig() {
	}

	@After("deleteConfig()")
	public void onDeleteConfig(JoinPoint jp) {
		String buildPlanItemId = (String) jp.getArgs()[0];
		log.info("After delete file {}, delete Build Plan Item on it", buildPlanItemId);
		buildPlanItemBiz.delete(buildPlanItemId);
	}

//	@Pointcut("execution(* com.boarsoft.boar.proj.biz.ProjBizImpl.delete(String))")
//	public void deleteProject() {
//		log.info("Point Cut com.boarsoft.boar.proj.biz.ProjBizImpl.delete");
//	}

//	@After("deleteProject()")
//	public void onDeleteProj(JoinPoint jp) {
//		String projId = (String) jp.getArgs()[0];
//		log.info("After delete project {}, delete server info of it", projId);
//		serverBiz.deleteByProjId(projId);
//	}

//	@Pointcut("execution(* com.boarsoft.boar.admin.biz.CorpBizImpl.delete(String))")
//	public void deleteCorp() {
//	}

//	@After("deleteCorp()")
//	public void onDeleteCorp(JoinPoint jp) {
//		String corpId = (String) jp.getArgs()[0];
//	}

//	@Pointcut("execution(* com.boarsoft.boar.admin.biz.DeptBizImpl.delete(String))")
//	public void deleteDept() {
//	}

	// @After("deleteDept()")
	// public void onDeleteDept(JoinPoint jp) {
	// //String deptId = (String) jp.getArgs()[0];
	// }

	public PlanItemBiz getBuildPlanItemBiz() {
		return buildPlanItemBiz;
	}

	public void setBuildPlanItemBiz(PlanItemBiz buildPlanItemBiz) {
		this.buildPlanItemBiz = buildPlanItemBiz;
	}
}
