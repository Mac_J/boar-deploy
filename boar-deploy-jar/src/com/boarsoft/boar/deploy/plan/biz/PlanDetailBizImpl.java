package com.boarsoft.boar.deploy.plan.biz;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.plan.PlanDetailBiz;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.hibernate.biz.SimpleBizImpl;

/**
 * 维护部署计划与节点信息关联中间表
 */
@Component
public class PlanDetailBizImpl extends SimpleBizImpl implements PlanDetailBiz {
	@Override
	@Transactional(readOnly = true)
	public PagedResult<BuildPlanDetail> list(String planId, String targetId, String env, String key, String orderBy, int pageNo,
			int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(BuildPlanDetail.class.getName());
		sb.append(" a where 1=1");
		if (Util.strIsNotEmpty(planId)) {
			sb.append(" and a.planId='").append(planId).append("'");
		}
		if (Util.strIsNotEmpty(targetId)) {
			sb.append(" and a.targetId='").append(targetId).append("'");
		}
		if (Util.strIsNotEmpty(env) && !"ALL".equalsIgnoreCase(env)) {
			sb.append(" and a.env='").append(env).append("'");
		}
		if (Util.strIsNotEmpty(key)) {
			sb.append(" and (a.code like '%").append(key);
			sb.append("%' or a.name like '%").append(key);
			sb.append("%' or a.memo like '%").append(key);
			sb.append("%') ");
		}
		int total = dao.getTotal("select count(a)".concat(sb.toString()));
		if (Util.strIsNotEmpty(orderBy)) {
			String ob = Util.array2str(("a.".concat(orderBy))//
					.replaceAll(",", ",").split(","), ",");
			sb.append(" order by ").append(ob);
		} else {
			sb.append(" order by a.name");
		}
		List<BuildPlanDetail> lt = dao.list("select a".concat(sb.toString()), pageNo, pageSize);
		return new PagedResult<>(total, lt, pageNo, pageSize);
	}

	@Override
	@Transactional(readOnly = true)
	public List<BuildPlanDetail> list(String planId, String targetId, String env) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(BuildPlanDetail.class.getName());
		sb.append(" a where 1=1");
		if (Util.strIsNotEmpty(planId)) {
			sb.append(" and a.planId='").append(planId).append("'");
		}
		if (Util.strIsNotEmpty(targetId)) {
			sb.append(" and a.targetId='").append(targetId).append("'");
		}
		if (Util.strIsNotEmpty(env) && !"ALL".equalsIgnoreCase(env)) {
			sb.append(" and a.env='").append(env).append("'");
		}
		return dao.list("select a".concat(sb.toString()));
	}

	@Override
	@Transactional
	public void save(BuildPlanDetail o) {
		if (Util.strIsEmpty(o.getId())) {
			dao.save(o);
		} else {
			dao.merge(o);
		}
	}

	/**
	 * 删除 plan 和 server 的关系
	 */
	@Override
	@Transactional
	public void delete(String id) {
		dao.delete(BuildPlanDetail.class, id);
	}

	@Override
	@Transactional(readOnly = true)
	public BuildPlanDetail get(String id) {
		return dao.get(BuildPlanDetail.class, id);
	}
}