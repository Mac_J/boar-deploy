package com.boarsoft.boar.deploy.plan.biz;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.app.AppInstBiz;
import com.boarsoft.boar.common.Constants;
import com.boarsoft.boar.deploy.app.AppDeployBiz;
import com.boarsoft.boar.deploy.entity.AppDeployInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.plan.PlanAppBiz;
import com.boarsoft.boar.deploy.plan.PlanItemBiz;
import com.boarsoft.boar.deploy.plan.PlanDetailBiz;
import com.boarsoft.boar.entity.AppInst;
import com.boarsoft.boar.entity.DirInfo;
import com.boarsoft.boar.entity.ServerInfo;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.hibernate.biz.SimpleBizImpl;

@Component("planAppBizImpl")
public class PlanAppBizImpl extends SimpleBizImpl implements PlanAppBiz {
	@Autowired
	private AppDeployBiz appBiz;
	@Autowired
	private PlanItemBiz planItemBiz;
	@Autowired
	private AppInstBiz appInstBiz;
	@Autowired
	private PlanDetailBiz planServerBiz;

	@Override
	@Transactional
	public ReplyInfo<Object> save(BuildPlanItem o) {
		String appId = o.getTargetId();
		// 冗余APP信息
		AppDeployInfo app = (AppDeployInfo) appBiz.get(appId);
		if (app == null) {
			return Constants.REPLY_ERROR_NOTFOUND;
		}
		o.setCode(app.getCode());
		o.setName(app.getName());
		o.setVer(app.getVer());
		// o.setDeployPath(app.getDeployPath());
		// o.setAppType(app.getType());
		o.setType(BuildPlanItem.TYPE_APP);
		// 配置类应用无需发版，只需检出源码
		// o.setPackPath(app.getPackPath());
		o.setSourcePath(app.getSourcePath());
		// o.setStatus("0");// 部署计划初始化状态
		ReplyInfo<Object> ro = planItemBiz.save(o);
		if (ro.isSuccess()) {
			// 查询app当前的部署信息，作为其在PlanServer中的初始数据
			List<AppInst> aiLt = appInstBiz.list(null, appId, null, null);
			for (AppInst ai : aiLt) {
				ServerInfo s = ai.getServer();
				BuildPlanDetail ps = new BuildPlanDetail();
				ps.setPlanId(o.getPlanId());
				ps.setCode(s.getCode());
				ps.setEnv(s.getEnv());
				ps.setGroup(s.getGroup());
				ps.setIp(s.getIp());
				ps.setName(s.getName());
				ps.setServerId(s.getId());
				ps.setTargetId(appId);
				// planServer.setTargetTempPath(appInst.getTempPath());
				ps.setTargetPath(new File(ai.getDeployPath()).getParent());
				ps.setTargetPort(ai.getPort());
				ps.setUser(ai.getUser());
				planServerBiz.save(ps);
			}
		}
		return ReplyInfo.SUCCESS;
	}

	/**
	 * 根据planId查询planId关联的targetId,排除这部分targetId相关的信息，返回剩余符合条件的List<AppInfo>
	 */
	@Override
	@Transactional(readOnly = true)
	public PagedResult<AppDeployInfo> list(String projId, String planId, boolean in, String key, String orderBy, int pageNo,
			int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(AppDeployInfo.class.getName())//
				.append(" a where 1=1");
		if (Util.strIsNotEmpty(projId)) {
			// 先查proj在DirTree的path
			DirInfo p = dao.get(DirInfo.class, projId);
			// 再查App所属Proj是p的子项目的
			sb.append(" and a.projId in (select d.id from ")//
					.append(DirInfo.class.getName())//
					.append(" d where d.type=").append(DirInfo.TYPE_PROJ)//
					.append(" and d.path like '").append(p.getPath()).append("%')");
		}
		sb.append(" and a.id ").append(in ? "in" : "not in")//
				.append(" (select t.targetId from ").append(BuildPlanItem.class.getName())//
				.append(" t where t.planId='").append(planId).append("' and type=")//
				.append(BuildPlanItem.TYPE_APP).append(")").toString();
		if (Util.strIsNotEmpty(key)) {
			sb.append(" and (a.code like '%").append(key);
			sb.append("%' or a.name like '%").append(key);
			sb.append("%' or a.memo like '%").append(key);
			sb.append("%')");
		}
		int total = dao.getTotal("select count(a.id)".concat(sb.toString()));
		List<AppDeployInfo> lt = dao.list("select a".concat(sb.toString()), pageNo, pageSize);
		return new PagedResult<AppDeployInfo>(total, lt, pageNo, pageSize);
	}
}