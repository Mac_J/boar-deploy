package com.boarsoft.boar.deploy.plan.biz;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.common.Constants;
import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.entity.FileDeployInfo;
import com.boarsoft.boar.deploy.plan.PlanFileBiz;
import com.boarsoft.boar.deploy.plan.PlanItemBiz;
import com.boarsoft.boar.entity.FileVersion;
import com.boarsoft.boar.file.FileVersionBiz;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.hibernate.biz.SimpleBizImpl;

@Component("planFileBizImpl")
public class PlanFileBizImpl extends SimpleBizImpl implements PlanFileBiz {

	@Autowired
	private FileVersionBiz fileVersionBiz;
	@Autowired
	private PlanItemBiz planItemBiz;

	@Override
	@Transactional(readOnly = true)
	public PagedResult<BuildPlanDetail> list(String projId, String planId, String appId, String key, String orderBy, int pageNo,
			int pageSize) {
		StringBuilder sb = new StringBuilder();
		if (Util.strIsNotEmpty(planId)) {
			sb.append(" and a.projId='").append(projId).append("'");
		}
		if (Util.strIsNotEmpty(planId)) {
			sb.append(" and a.planId='").append(planId).append("'");
		}
		if (Util.strIsNotEmpty(appId)) {
			sb.append(" and a.appId='").append(appId).append("'");
		}
		if (Util.strIsNotEmpty(key)) {
			sb.append(" and (a.code like '%").append(key);
			sb.append("%' or a.name like '%").append(key);
			sb.append("%' or a.memo like '%").append(key);
			sb.append("%' or a.version like '%").append(key);
			sb.append("%')");
		}
		String wl = new StringBuilder().append("select count(a) from ").append(BuildPlanDetail.class.getName()).append(" a ")
				.append(" where 1=1 ").append(sb).toString();
		int total = dao.getTotal(wl);
		if (Util.strIsNotEmpty(orderBy)) {
			String ob = Util.array2str(("a." + orderBy).replaceAll(",", ",").split(","), ",");
			sb.append(" order by ").append(ob);
		} else {
			sb.append(" order by a.name");
		}
		String ql = new StringBuilder().append("select a from ").append(BuildPlanDetail.class.getName()).append(" a ")
				.append(" where 1=1 ").append(sb).toString();
		List<BuildPlanDetail> lt = dao.list(ql, pageNo, pageSize);
		return new PagedResult<BuildPlanDetail>(total, lt, pageNo, pageSize);
	}

	@Override
	@Transactional
	public ReplyInfo<Object> save(BuildPlanItem o) {
		FileVersion fv = fileVersionBiz.get(o.getTargetId());//
		if (fv == null) {
			return Constants.REPLY_ERROR_NOTFOUND;
		}
		FileDeployInfo fi = (FileDeployInfo) fv.getFileInfo();
		if (fi == null) {
			return Constants.REPLY_ERROR_NOTFOUND;
		}
		// 保存每条关联关系前需要先根据targetId查询其对应的code, name, version, path信息
		o.setCode(fi.getCode());
		o.setName(fi.getName());
		o.setVer(fv.getVer());
		o.setLastTime(Util.getStdmfDateTime());
		o.setMemo(fi.getMemo());
		if (Util.strIsEmpty(o.getId())) {
			o.setCreateTime(Util.getStdmfDateTime());
			// 插入前先判重：如果buildPlanDetail的planId，targetId已存在则不再插入
			if (planItemBiz.exists(o.getPlanId(), o.getTargetId())) {
				return Constants.REPLY_INFO_DUPLICATED;
			}
			planItemBiz.save(o);
		} else {
			dao.merge(o);
		}
		return ReplyInfo.SUCCESS;
	}
}