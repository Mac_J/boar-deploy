package com.boarsoft.boar.deploy.plan;

import com.boarsoft.boar.deploy.entity.BuildPlan;
import com.boarsoft.common.dao.PagedResult;

public interface PlanBiz {
	/**
	 * 查询现有部署计划总览列表 以PagedResult<DeployPlanInfo>形式返回
	 *
	 * @param projId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<BuildPlan> list(String projId, String key, String orderBy, int pageNo, int pageSize);

	/**
	 * 增加一个部署计划
	 */
	void save(BuildPlan o);

	/**
	 * 删除某个部署计划，需同步删除其关联的部署详情表（BuildPlanDetail）信息
	 *
	 * @param id
	 */
	void delete(String id);

	/**
	 * 更新一条部署计划总览
	 */
	void update(BuildPlan o);

	/**
	 * 获取部署信息
	 *
	 * @param id
	 * @return
	 */
	BuildPlan get(String id);

	/**
	 * 启用部署计划
	 *
	 * @param id
	 * @return
	 */
	boolean start(String id);

	/**
	 * 停用部署计划
	 *
	 * @param id
	 * @return
	 */
	boolean stop(String id);
}