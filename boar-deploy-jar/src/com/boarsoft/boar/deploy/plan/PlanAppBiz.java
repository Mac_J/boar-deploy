package com.boarsoft.boar.deploy.plan;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.AppDeployInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.common.dao.PagedResult;

public interface PlanAppBiz {
	/**
	 * 
	 * @param projId
	 * @param planId
	 * @param in
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<AppDeployInfo> list(String projId, String planId, boolean in, String key, String orderBy, int pageNo, int pageSize);

	/**
	 * 
	 * @param o
	 */
	ReplyInfo<Object> save(BuildPlanItem o);
}