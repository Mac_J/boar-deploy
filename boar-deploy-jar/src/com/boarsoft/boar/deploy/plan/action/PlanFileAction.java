package com.boarsoft.boar.deploy.plan.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.LogonI;
import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.PlanFileBiz;
import com.boarsoft.common.Authorized;
import com.boarsoft.web.login.Logined;

@RestController
@RequestMapping("/plan/file")
public class PlanFileAction {
	private static final Logger log = LoggerFactory.getLogger(PlanFileAction.class);

	@Autowired
	private PlanFileBiz planFileBiz;

	@RequestMapping("/save.do")
	@Authorized(code = "deploy.plan.item.save")
	public ReplyInfo<Object> save(@Logined LogonI<String> logon, BuildPlanItem o, String targetIds) {
		for (String targetId : targetIds.split(",")) {
			o.setTargetId(targetId);
			o.setOwnerId(logon.getId());
			ReplyInfo<Object> ro = planFileBiz.save(o);
			if (ro.isSuccess()) {
				continue;
			}
			log.error("Error on save build plan file item, reason: {}", ro.getData());
			return ro;
		}
		return ReplyInfo.SUCCESS;
	}
}