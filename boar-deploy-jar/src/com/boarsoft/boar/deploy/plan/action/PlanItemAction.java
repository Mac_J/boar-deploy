package com.boarsoft.boar.deploy.plan.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.PlanItemBiz;
import com.boarsoft.common.Authorized;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.web.controller.BaseController;

@RestController
@RequestMapping("/plan/item")
public class PlanItemAction extends BaseController {
	// private static final Logger log =
	// LoggerFactory.getLogger(PlanItemAction.class);

	@Autowired
	private PlanItemBiz planItemBiz;

	@RequestMapping("/delete.do")
	@Authorized(code = "deploy.plan.item.delete")
	public ReplyInfo<Object> delete(String id) {
		planItemBiz.delete(id);
		return ReplyInfo.SUCCESS;
	}

	/**
	 * 
	 * @param projId
	 * @param planId
	 * @param type
	 * @param targetId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/list.do")
	@Authorized(code = "deploy.plan.item.list")
	public ReplyInfo<Object> list(String projId, String planId, Short type, String targetId, String key, String orderBy,
			int pageNo, int pageSize) {
		PagedResult<BuildPlanItem> pr = planItemBiz.list(//
				planId, type, targetId, key, orderBy, pageNo, pageSize);
		for (BuildPlanItem a : pr.getList()) {
			if (Util.strIsEmpty(a.getOwnerId())) {
				continue;
			}
			a.setOwner(this.getSysUser(a.getOwnerId()));
		}
		return new ReplyInfo<Object>(true, pr);
	}

	/**
	 * 
	 * @param projId
	 * @param planId
	 * @param type
	 * @param targetId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/list2.do")
	@Authorized(code = "deploy.plan.item.list")
	public ReplyInfo<Object> list(String planId, String key, String orderBy, int pageNo, int pageSize) {
		PagedResult<BuildPlanItem> pr = planItemBiz.list2(planId, key, orderBy, pageNo, pageSize);
		for (BuildPlanItem a : pr.getList()) {
			if (Util.strIsEmpty(a.getOwnerId())) {
				continue;
			}
			a.setOwner(this.getSysUser(a.getOwnerId()));
		}
		return new ReplyInfo<Object>(true, pr);
	}

	@RequestMapping("/get.do")
	@Authorized(code = "deploy.plan.item.get")
	public ReplyInfo<Object> get(String id) {
		BuildPlanItem o = planItemBiz.get(id);
		return new ReplyInfo<Object>(true, o);
	}
}