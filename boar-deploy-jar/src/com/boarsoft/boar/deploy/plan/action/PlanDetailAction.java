package com.boarsoft.boar.deploy.plan.action;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.app.AppInstBiz;
import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.plan.PlanDetailBiz;
import com.boarsoft.boar.entity.AppInst;
import com.boarsoft.boar.entity.ServerInfo;
import com.boarsoft.boar.server.ServerBiz;
import com.boarsoft.common.Authorized;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.web.controller.BaseController;

/**
 * 部署计划中的（待部署）服务器管理
 *
 * @author Mac_J
 */
@RestController
@RequestMapping("/plan/detail")
public class PlanDetailAction extends BaseController {
	private static final Logger log = LoggerFactory.getLogger(PlanDetailAction.class);

	@Autowired
	private AppInstBiz appInstBiz;
	@Autowired
	private ServerBiz serverBiz;
	@Autowired
	private PlanDetailBiz planDetailBiz;

	/**
	 * 页面右侧对已绑定的服务器列表的查询
	 * 
	 * @param planId
	 * @param targetId
	 * @param env
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("/list.do")
	@Authorized(code = "deploy.plan.detail.list")
	public ReplyInfo<Object> list(String planId, String targetId, String env, String key, String orderBy, int pageNo,
			int pageSize) {
		PagedResult<BuildPlanDetail> pr = planDetailBiz.list(planId, targetId, env, key, orderBy, pageNo, pageSize);
		return new ReplyInfo<Object>(true, pr);
	}

	/**
	 * 批量保存BuildPlan和BuildPlanDetail的关联关系需要冗余Server的信息
	 */
	@RequestMapping("/save.do")
	@Authorized(code = "deploy.plan.detail.save")
	public ReplyInfo<Object> save(BuildPlanDetail o, String serverIds) {
		String appId = o.getTargetId();
		// 如果需要冗余的保存多条server信息到deploy_plan_server表，则需先查询这些信息
		for (String sid : serverIds.split(",")) {
			AppInst ai = appInstBiz.find(appId, sid);
			if (ai == null) {
				log.warn("App instance {}/{} is missing", appId, sid);
//				AppDeployInfo app = (AppDeployInfo) appBiz.get(appId);
//				ai = new AppInst();
//				ai.setAppId(appId);
//				ai.setDeployPath(app.getDeployPath());
//				ai.setEnv(o.getEnv());
//				ai.setGroup(o.getGroup());
//				ai.setIp(o.getIp());
//				ai.setNo(o.getNo);
				continue;
			} else {
				if (Util.strIsEmpty(o.getTempPath())) {
					o.setTempPath(ai.getTempPath());
				} else {
					o.setTempPath(o.getTempPath());
				}
				if (Util.strIsEmpty(o.getTargetPath())) {
					o.setTargetPath(new File(ai.getDeployPath()).getParent());
				} else {
					o.setTargetPath(o.getTargetPath());
				}
				if (0 == o.getTargetPort()) {
					o.setTargetPort(ai.getPort());
				} else {
					o.setTargetPort(o.getTargetPort());
				}
				if (Util.strIsEmpty(o.getUser())) {
					o.setUser(ai.getUser());
				} else {
					o.setUser(o.getUser());
				}
			}
			ServerInfo s = serverBiz.get(sid);
			o.setServerId(sid);
			o.setCode(s.getCode());
			o.setEnv(s.getEnv());
			o.setGroup(s.getGroup());
			o.setIp(s.getIp());
			o.setName(s.getName());
			planDetailBiz.save(o);
		}
		return ReplyInfo.SUCCESS;
	}

	/**
	 * 删除一条plan与server的关联关系 根据planId，targetId，serverId,删除deploy_plan_server一条信息
	 */
	@RequestMapping("/delete.do")
	@Authorized(code = "deploy.plan.detail.delete")
	public ReplyInfo<Object> delete(String id) {
		planDetailBiz.delete(id);
		return ReplyInfo.SUCCESS;
	}
}