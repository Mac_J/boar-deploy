package com.boarsoft.boar.deploy.plan.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.common.Constants;
import com.boarsoft.boar.deploy.common.BuildContext;
import com.boarsoft.boar.deploy.entity.BuildPlan;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.plan.PlanBiz;
import com.boarsoft.boar.deploy.plan.PlanItemBiz;
import com.boarsoft.common.Authorized;
import com.boarsoft.flow.core.SimpleFlow;
import com.boarsoft.flow.core.SimpleFlowEngine;

/**
 *
 * @author Mac_J
 */
@RestController
@RequestMapping("/plan/pack")
public class PlanPackAction {
	private static final Logger log = LoggerFactory.getLogger(PlanPackAction.class);

	@Autowired
	private SimpleFlowEngine flowEngine;
	@Autowired
	private PlanBiz planBiz;
	@Autowired
	private PlanItemBiz planItemBiz;

	@RequestMapping("/start.do")
	@Authorized(code = "deploy.plan.pack")
	public ReplyInfo<Object> pack(String planId, String targetIds, short policy, String sessionId, String env) {
		BuildPlan plan = planBiz.get(planId);
		String dc = plan.getPackCode();
		if (flowEngine.exists(dc)) {
			// 构造数据
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("planId", planId);
			// 查询用户指定的要打包的项（应用）
			List<BuildPlanItem> items = new ArrayList<BuildPlanItem>();
			for (String targetId : targetIds.split(",")) {
				BuildPlanItem a = planItemBiz.get(targetId);
				items.add(a);
			}
			data.put("items", items);
			//
			String flowId = dc;
			SimpleFlow flow = flowEngine.create(flowId);
			flow.setData(data);
			if (BuildContext.putBuild(flowId, flow)) {
				try {
					flowEngine.start(flow);
					return ReplyInfo.SUCCESS;
				} catch (Throwable throwable) {
					log.error("Error on start flow {}", flow);
					return new ReplyInfo<Object>(Constants.ERROR_INTERNAL);
				}
			}
			return new ReplyInfo<Object>(String.format(//
					"Last build of plan %s is still running", flowId));
		}
		// TODO 调用JENKINS
		return ReplyInfo.SUCCESS;
	}
}