package com.boarsoft.boar.deploy.plan;

import java.util.List;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.common.dao.PagedResult;

public interface PlanItemBiz {
	/**
	 * 保存或修改单个Item，此方法会检查相同的item是否已存在
	 * 
	 * @param o
	 * @return
	 */
	ReplyInfo<Object> save(BuildPlanItem o);

	/**
	 * 删除某个Item<br>
	 * 当文件类型为sql和shell时，需同步删除添加时时创建的FileInfo/FileVersion
	 * 
	 * @param id
	 */
	void delete(String id);

	/**
	 * 获取Item信息
	 * 
	 * @param id
	 * @return BuildPlanItem
	 */
	BuildPlanItem get(String id);

	/**
	 * 导入item信息，并修改plan的状态为草稿
	 * 
	 * @param planId
	 * @param lt
	 */
	void save(String planId, List<BuildPlanItem> lt);

	BuildPlanItem find(String planId, String code);

	void update(BuildPlanItem o);

	boolean exists(String planId, String targetId);

	/**
	 * 查询所有手工添加部署项
	 * 
	 * @param planId
	 * @param type
	 * @param targetId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<BuildPlanItem> list(String planId, Short type, String targetId, String key, String orderBy, int pageNo,
			int pageSize);

	/**
	 * 
	 * @param planId
	 * @param type
	 * @param targetId
	 * @param orderBy
	 * @return
	 */
	List<BuildPlanItem> list(String planId, Short type, String targetId, String orderBy);

	/**
	 * 只查询可部署项（APP/SQL/SHELL三种类型）
	 * 
	 * @param planId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<BuildPlanItem> list2(String planId, String key, String orderBy, int pageNo, int pageSize);

	/**
	 * 
	 * @param planId
	 * @return
	 */
	List<BuildPlanItem> list2(String planId);
}