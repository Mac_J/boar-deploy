package com.boarsoft.boar.deploy.plan;

import java.util.List;

import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.common.dao.PagedResult;

/**
 * 维护部署计划与节点信息关联中间表
 */
public interface PlanDetailBiz {

	/**
	 * 新增一条plan和server的关联关系信息
	 */
	void save(BuildPlanDetail o);

	/**
	 * 删除一条plan和server的关联关系信息
	 */
	void delete(String id);

	BuildPlanDetail get(String id);

	PagedResult<BuildPlanDetail> list(String planId, String targetId, String env, String key, String orderBy, int pageNo,
			int pageSize);

	List<BuildPlanDetail> list(String planId, String targetId, String env);
}
