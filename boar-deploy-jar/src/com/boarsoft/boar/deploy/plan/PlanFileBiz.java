package com.boarsoft.boar.deploy.plan;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.BuildPlanDetail;
import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.common.dao.PagedResult;

public interface PlanFileBiz {

	/**
	 * 
	 * @param projId
	 * @param planId
	 * @param appId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<BuildPlanDetail> list(String projId, String planId, String appId, String key, String orderBy, int pageNo,
			int pageSize);

	/**
	 * 
	 * @param o
	 * @return
	 */
	ReplyInfo<Object> save(BuildPlanItem o);
}