package com.boarsoft.boar.deploy.websocket;

public class DeployParametersInfo {

	private String call_back_url;
	private String app_snap;
	private String app_vsn;
	private String app_upload;
	private String app_ip;
	private String app_home;
	private String app_name;

	private String build_index;
	private String target_id;
	private String target_dir;
	private String app_sbin;
	private String job_id;
	private String deploy_type;
	private String cpl_env;
	private String app_usr;
	public String getCall_back_url() {
		return call_back_url;
	}
	public void setCall_back_url(String call_back_url) {
		this.call_back_url = call_back_url;
	}
	public String getApp_snap() {
		return app_snap;
	}
	public void setApp_snap(String app_snap) {
		this.app_snap = app_snap;
	}
	public String getApp_vsn() {
		return app_vsn;
	}
	public void setApp_vsn(String app_vsn) {
		this.app_vsn = app_vsn;
	}
	public String getApp_upload() {
		return app_upload;
	}
	public void setApp_upload(String app_upload) {
		this.app_upload = app_upload;
	}
	public String getApp_ip() {
		return app_ip;
	}
	public void setApp_ip(String app_ip) {
		this.app_ip = app_ip;
	}
	public String getApp_home() {
		return app_home;
	}
	public void setApp_home(String app_home) {
		this.app_home = app_home;
	}
	public String getApp_name() {
		return app_name;
	}
	public void setApp_name(String app_name) {
		this.app_name = app_name;
	}
	public String getBuild_index() {
		return build_index;
	}
	public void setBuild_index(String build_index) {
		this.build_index = build_index;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getTarget_dir() {
		return target_dir;
	}
	public void setTarget_dir(String target_dir) {
		this.target_dir = target_dir;
	}
	public String getApp_sbin() {
		return app_sbin;
	}
	public void setApp_sbin(String app_sbin) {
		this.app_sbin = app_sbin;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getDeploy_type() {
		return deploy_type;
	}
	public void setDeploy_type(String deploy_type) {
		this.deploy_type = deploy_type;
	}
	public String getCpl_env() {
		return cpl_env;
	}
	public void setCpl_env(String cpl_env) {
		this.cpl_env = cpl_env;
	}
	public String getApp_usr() {
		return app_usr;
	}
	public void setApp_usr(String app_usr) {
		this.app_usr = app_usr;
	}
	
}
