package com.boarsoft.boar.deploy.websocket;

public class DeployURLEvent {
	private String name;
	private String url;
	private DeployBuildInfo build;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public DeployBuildInfo getBuild() {
		return build;
	}
	public void setBuild(DeployBuildInfo build) {
		this.build = build;
	}
	
}
