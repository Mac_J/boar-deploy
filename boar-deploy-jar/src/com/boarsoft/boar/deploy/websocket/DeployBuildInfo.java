package com.boarsoft.boar.deploy.websocket;

public class DeployBuildInfo extends BaseBuildInfo{
	private DeployParametersInfo parameters;
	
	public DeployParametersInfo getParameters() {
		return parameters;
	}
	public void setParameters(DeployParametersInfo parameters) {
		this.parameters = parameters;
	}
	
	
}
