package com.boarsoft.boar.deploy.websocket;

/**
 * 前端websocket部署进度反馈msg
 * */
public class DeployEventMSG extends BaseEventMSG{
	private String[] deployTypes = {"0-部署单个应用包","1-部署单个应用包中的bin包","2-部署单个应用包中的lib包","3-停单个应用","4-启单个应用","5-重启单个应用"};
	private String presentDeployType;
	private String[] deploySteps={"0-初始化"," 1-传输jar包或配置文件","2-部署"};
	private int presentDeployStep;
	private String ip;
	/**
	 * 全部阶段
	 * */
	private int totalSteps = deploySteps.length;
	
	public DeployEventMSG(String jobId,String targetId,String status,String presentDeployType,int presentDeployStep){
		super.jobId = jobId;
		super.targetId = targetId;
		super.status = status;
		this.presentDeployType = presentDeployType;
		this.presentDeployStep = presentDeployStep;
		super.progress = (presentDeployStep+1)+"/"+getTotalSteps();
	}
	

//	public static void main(String[] args) {
//		DeployMSG deployMsg = new DeployMSG("jobId","40286b81542935a101542942a9ea0002","0","0",1);
//		System.out.println(JSON.toJSONString(deployMsg));
//	}

	public String[] getDeployTypes() {
		return deployTypes;
	}

	public void setDeployTypes(String[] deployTypes) {
		this.deployTypes = deployTypes;
	}

	public int getPresentDeployStep() {
		return presentDeployStep;
	}

	public void setPresentDeployStep(int presentDeployStep) {
		this.presentDeployStep = presentDeployStep;
	}

	public String getPresentDeployType() {
		return presentDeployType;
	}

	public void setPresentDeployType(String presentDeployType) {
		this.presentDeployType = presentDeployType;
	}

	public int getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
}
