package com.boarsoft.boar.deploy.websocket;

public class CompileURLEvent {
	private String name;
	private String url;
	private CompileBuildInfo build;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public CompileBuildInfo getBuild() {
		return build;
	}
	public void setBuild(CompileBuildInfo build) {
		this.build = build;
	}
	
}
