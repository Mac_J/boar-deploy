package com.boarsoft.boar.deploy.websocket;

import com.boarsoft.web.websocket.WebsocketMessage;

public class BaseEventMSG extends WebsocketMessage {

    protected String eventName;
    protected String targetId;
    /**
     * 0-成功，1-失败继续  2-失败终止
     */
    protected String status;
    protected String progress;
    private String sessionId;
    private String env;
    protected String jobId;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
