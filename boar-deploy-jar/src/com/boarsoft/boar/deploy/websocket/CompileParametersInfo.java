package com.boarsoft.boar.deploy.websocket;

public class CompileParametersInfo {
	private String sourcePath;
	private String call_back_url;
	private String src_revision;
	private String source_root;
	private String app_vsn;
	private String svn_pass;
	private String mvn_comm;
	private String build_index;

	private String target_id;
	private String target_dir;
	private String tfs_defa_coll;
	private String project_root;
	private String package_type;
	private String compile_type;
	private String svn_user;
	private String local_src_dir;
	private String src_dir;
	private String job_id;
	private String cpl_env;
	
	
	public String getCall_back_url() {
		return call_back_url;
	}
	public void setCall_back_url(String call_back_url) {
		this.call_back_url = call_back_url;
	}
	public String getSrc_revision() {
		return src_revision;
	}
	public void setSrc_revision(String src_revision) {
		this.src_revision = src_revision;
	}
	public String getApp_vsn() {
		return app_vsn;
	}
	public void setApp_vsn(String app_vsn) {
		this.app_vsn = app_vsn;
	}
	public String getMvn_comm() {
		return mvn_comm;
	}
	public void setMvn_comm(String mvn_comm) {
		this.mvn_comm = mvn_comm;
	}
	public String getBuild_index() {
		return build_index;
	}
	public void setBuild_index(String build_index) {
		this.build_index = build_index;
	}
	public String getTarget_id() {
		return target_id;
	}
	public void setTarget_id(String target_id) {
		this.target_id = target_id;
	}
	public String getTarget_dir() {
		return target_dir;
	}
	public void setTarget_dir(String target_dir) {
		this.target_dir = target_dir;
	}
	public String getProject_root() {
		return project_root;
	}
	public void setProject_root(String project_root) {
		this.project_root = project_root;
	}
	public String getPackage_type() {
		return package_type;
	}
	public void setPackage_type(String package_type) {
		this.package_type = package_type;
	}
	public String getCompile_type() {
		return compile_type;
	}
	public void setCompile_type(String compile_type) {
		this.compile_type = compile_type;
	}
	public String getLocal_src_dir() {
		return local_src_dir;
	}
	public void setLocal_src_dir(String local_src_dir) {
		this.local_src_dir = local_src_dir;
	}
	public String getSrc_dir() {
		return src_dir;
	}
	public void setSrc_dir(String src_dir) {
		this.src_dir = src_dir;
	}
	public String getJob_id() {
		return job_id;
	}
	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}
	public String getCpl_env() {
		return cpl_env;
	}
	public void setCpl_env(String cpl_env) {
		this.cpl_env = cpl_env;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getSource_root() {
		return source_root;
	}

	public void setSource_root(String source_root) {
		this.source_root = source_root;
	}

	public String getSvn_pass() {
		return svn_pass;
	}

	public void setSvn_pass(String svn_pass) {
		this.svn_pass = svn_pass;
	}

	public String getTfs_defa_coll() {
		return tfs_defa_coll;
	}

	public void setTfs_defa_coll(String tfs_defa_coll) {
		this.tfs_defa_coll = tfs_defa_coll;
	}

	public String getSvn_user() {
		return svn_user;
	}

	public void setSvn_user(String svn_user) {
		this.svn_user = svn_user;
	}
}
