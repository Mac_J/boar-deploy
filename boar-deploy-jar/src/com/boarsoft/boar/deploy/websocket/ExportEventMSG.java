package com.boarsoft.boar.deploy.websocket;

/**
 * 前端websocket编译打包进度反馈msg
 * */
public class ExportEventMSG extends BaseEventMSG {

	private String[] compileTypes = {"0-完整包","1-程序包","2-配置包"};
	private String presentExportType;
	private String[] exportSteps = {"0-初始化","1-下载源码","2-收尾"};
	private int presentExportStep;
	private int totalSteps = exportSteps.length;
	private int jobCount;

	public ExportEventMSG(int jobCount,String jobId, String targetId, String status, String presentExportType, int presentExportStep){
		this.jobCount = jobCount;
		super.eventName="export";
		super.jobId = jobId;
		super.targetId = targetId;
		super.status = status;
		this.presentExportType = presentExportType;
		this.presentExportStep = presentExportStep;
		super.progress = (presentExportStep+1)+"/"+getTotalSteps();
	}
	
//	public static void main(String[] args) {
//		CompileEventMSG compileMsg = new CompileEventMSG("jobId","40286b81542935a101542942a9ea0002","0","0",1);
//		System.out.println(JSON.toJSONString(compileMsg));
//	}
	
	public String[] getCompileTypes() {
		return compileTypes;
	}
	public void setCompileTypes(String[] compileTypes) {
		this.compileTypes = compileTypes;
	}
	public String getPresentExportType() {
		return presentExportType;
	}
	public void setPresentExportType(String presentExportType) {
		this.presentExportType = presentExportType;
	}
	public String[] getExportSteps() {
		return exportSteps;
	}
	public void setExportSteps(String[] exportSteps) {
		this.exportSteps = exportSteps;
	}
	public int getPresentExportStep() {
		return presentExportStep;
	}
	public void setPresentExportStep(int presentExportStep) {
		this.presentExportStep = presentExportStep;
	}
	public int getTotalSteps() {
		return totalSteps;
	}
	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	public int getJobCount() {
		return jobCount;
	}

	public void setJobCount(int jobCount) {
		this.jobCount = jobCount;
	}
}
