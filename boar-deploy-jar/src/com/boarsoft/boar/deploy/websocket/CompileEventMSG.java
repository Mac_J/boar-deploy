package com.boarsoft.boar.deploy.websocket;

/**
 * 前端websocket编译打包进度反馈msg
 * */
public class CompileEventMSG extends BaseEventMSG {
	
	private String[] compileTypes = {"0-完整包","1-程序包","2-配置包"};
	private String presentCompileType;
	private String[] compileSteps = {"0-初始化","1-下载源码","2-编译"};
	private int presentCompileStep;
	private int totalSteps = compileSteps.length;
	
	public CompileEventMSG(String jobId,String targetId,String status,String presentCompileType,int presentCompileStep){
		super.eventName="compile";
		super.jobId = jobId;
		super.targetId = targetId;
		super.status = status;
		this.presentCompileType = presentCompileType;
		this.presentCompileStep = presentCompileStep;
		super.progress = (presentCompileStep+1)+"/"+getTotalSteps();
	}
	
//	public static void main(String[] args) {
//		CompileEventMSG compileMsg = new CompileEventMSG("jobId","40286b81542935a101542942a9ea0002","0","0",1);
//		System.out.println(JSON.toJSONString(compileMsg));
//	}
	
	public String[] getCompileTypes() {
		return compileTypes;
	}
	public void setCompileTypes(String[] compileTypes) {
		this.compileTypes = compileTypes;
	}
	public String getPresentCompileType() {
		return presentCompileType;
	}
	public void setPresentCompileType(String presentCompileType) {
		this.presentCompileType = presentCompileType;
	}
	public String[] getCompileSteps() {
		return compileSteps;
	}
	public void setCompileSteps(String[] compileSteps) {
		this.compileSteps = compileSteps;
	}
	public int getPresentCompileStep() {
		return presentCompileStep;
	}
	public void setPresentCompileStep(int presentCompileStep) {
		this.presentCompileStep = presentCompileStep;
	}
	public int getTotalSteps() {
		return totalSteps;
	}
	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}

	
}
