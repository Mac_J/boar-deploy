package com.boarsoft.boar.deploy.websocket;

public class CompileBuildInfo extends BaseBuildInfo{
	private CompileParametersInfo parameters;
	
	
	public CompileParametersInfo getParameters() {
		return parameters;
	}
	public void setParameters(CompileParametersInfo parameters) {
		this.parameters = parameters;
	}
	
	
}
