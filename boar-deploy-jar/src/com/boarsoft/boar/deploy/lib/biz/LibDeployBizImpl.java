package com.boarsoft.boar.deploy.lib.biz;

import org.springframework.stereotype.Component;

import com.boarsoft.boar.deploy.entity.LibDeployInfo;
import com.boarsoft.boar.lib.LibBiz;
import com.boarsoft.boar.lib.biz.LibBizImpl;

@Component("libDeployBiz")
public class LibDeployBizImpl extends LibBizImpl implements LibBiz {
	public LibDeployBizImpl() {
		libInfoClazz = LibDeployInfo.class;
	}
}
