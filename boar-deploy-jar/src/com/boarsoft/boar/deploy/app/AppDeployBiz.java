package com.boarsoft.boar.deploy.app;

import com.boarsoft.boar.entity.AppInfo;

public interface AppDeployBiz {
	
	/**
	 * 获取应用信息
	 *
	 * @param id-应用id
	 * @return AppInfo
	 */
	AppInfo get(String id);
}
