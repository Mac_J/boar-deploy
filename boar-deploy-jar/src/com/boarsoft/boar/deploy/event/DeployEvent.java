package com.boarsoft.boar.deploy.event;

import java.io.Serializable;

/**
 * 
 * @author Mac_J
 *
 */
public class DeployEvent implements Serializable {
	private static final long serialVersionUID = 7343744212615081833L;

	/** 部署任务ID */
	protected String requestId;
	/** 实例ID（如果有的话） */
	protected String instId;
	/** 实例地址 */
	protected String addr;
	/** 实例的状态 */
	protected short status;
	/** 报告内容（异常信息） */
	protected String memo;
	
	protected String from;
	protected String fromAddr;

	@Override
	public String toString() {
		return new StringBuilder().append(requestId).append("/")//
				.append(instId).append("/").append(addr).append("=")//
				.append(status).append("/").append(memo).toString();
	}

	public String getInstId() {
		return instId;
	}

	public void setInstId(String instId) {
		this.instId = instId;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getFromAddr() {
		return fromAddr;
	}

	public void setFromAddr(String fromAddr) {
		this.fromAddr = fromAddr;
	}
}