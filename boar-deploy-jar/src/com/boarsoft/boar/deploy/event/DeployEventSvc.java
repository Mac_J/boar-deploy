package com.boarsoft.boar.deploy.event;

public interface DeployEventSvc {

	void on(DeployEvent e) throws Exception;

}
