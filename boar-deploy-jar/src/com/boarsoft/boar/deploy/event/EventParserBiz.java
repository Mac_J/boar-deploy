package com.boarsoft.boar.deploy.event;

import javax.servlet.http.HttpServletRequest;

import com.boarsoft.boar.job.BaseExportJob;

public interface EventParserBiz {

	/**
	 * 解析导出源码回调参数
	 */
	void parseExportCallBack(HttpServletRequest req, BaseExportJob exportJob);

	/**
	 * 解析编译回调参数 把post请求参数拆开分别解析
	 */
	void parseCompileCallBack(HttpServletRequest req, BaseExportJob exportJob);

	/**
	 * 解析部署回调参数 把post请求参数拆开分别解析
	 */
	void parseDeployCallBack(HttpServletRequest req);
}
