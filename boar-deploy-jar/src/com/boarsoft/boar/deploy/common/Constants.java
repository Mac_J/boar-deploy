package com.boarsoft.boar.deploy.common;

public class Constants {
	/*
	 * target id;
	 */
	public final static String TARGET_ID = "target_id";

	/*
	 * 回调url;
	 */
	public final static String CALL_BACK_URL = "call_back_url";

	/*
	 * 部署job id;
	 */
	public final static String JOB_ID = "job_id";

	/*
	 * 构建index
	 */
	public final static String BUILD_INDEX = "build_index";

	/*
	 * 编译环境
	 */
	public final static String CPL_ENV = "cpl_env";

	/*
	 * 应用名称, ebc-busi-acc-service
	 */
	public final static String APP_NAME = "app_name";

	/*
	 * 应用版本号, 0.0.1
	 */
	public final static String APP_VSN = "app_vsn";

	/*
	 * SNAPSHOT
	 */
	public final static String APP_SNAP = "app_snap";

	/*
	 * 应用ip列表
	 */
	public final static String APP_IP = "app_ip";

	/*
	 * 远程应用服务器用户
	 */
	public final static String APP_USR = "app_usr";

	/*
	 * 应用包上传至远程应用服务器上目录
	 */
	public final static String APP_UPLOAD = "app_upload";

	/*
	 * 应用服务器上部署脚本所在目录
	 */
	public final static String APP_SBIN = "app_sbin";

	/*
	 * 应用在应用服务器上部署目录
	 */
	public final static String APP_HOME = "app_home";

	public final static String DEPLOY_TYPE = "deploy_type";

	/**
	 * 部署类型: 0 -- 部署单个应用包
	 */
	public final static int DEPLOY_TYPE_FULL = 0;
	/**
	 * 部署类型: 1 -- 部署单个应用包中的lib包
	 */
	public final static int DEPLOY_TYPE_LIBS = 1;
	/**
	 * 部署类型: 2 -- 部署单个应用包中的配置包
	 */
	public final static int DEPLOY_TYPE_CONF = 2;
	/**
	 * 部署类型: 3 -- 停单个应用
	 */
	public final static int DEPLOY_TYPE_STOP = 3;
	/**
	 * 部署类型: 4 -- 启单个应用
	 */
	public final static int DEPLOY_TYPE_START = 4;
	/**
	 * 部署类型: 5 -- 重启单个应用
	 */
	public final static int DEPLOY_TYPE_RESTART = 5;
	/**
	 * 部署类型: 6 -- 部署jar包列表
	 */
	public final static int DEPLOY_TYPE_JAR_LIST = 6;

	/*
	 * 应用在应用服务器上部署目录
	 */
	public final static String JAR_LIST = "jar_list";

	/*
	 * 部署标志: 
	 * 0 -- 顺序部署
	 * 1 -- 半数并发
	 * 2 -- 全量并发
	 */
	public final static String DEPLOY_FLAG_0 = "0";
	public final static String DEPLOY_FLAG_1 = "1";
	public final static String DEPLOY_FLAG_2 = "2";

	public final static String DEPLOY_CREATE_JOB_VAL = "job";
	public final static String DEPLOY_CREATE_BUILD_VAL = "buildWithParameters?token=helloworld";
	public final static String DEPLOY_CREATE_ITEM_VAL = "view/All/createItem";
	public final static String DEPLOY_CREATE_DISABLE = "disable";
	public final static String DEPLOY_CREATE_ENABLE = "enable";

	public final static String DEPLOY_CREATE_NAME_KEY = "name";
	public final static String DEPLOY_CREATE_MODE_KEY = "mode";
	public final static String DEPLOY_CREATE_MODE_VAL = "copy";
	public final static String DEPLOY_CREATE_FROM_KEY = "from";
	public final static String DEPLOY_CREATE_FROM_VAL = "deploy";
	public final static String COMPILE_CREATE_FROM_VAL = "compile_svn";
	public final static String DEPLOY_STATUS_KEY = "deploy_status";
	public final static String DEPLOY_STATUS_A = "A"; //初始
	public final static String DEPLOY_STATUS_0 = "0"; //构建成功
	public final static String DEPLOY_STATUS_1 = "1"; //失败继续
	public final static String DEPLOY_STATUS_2 = "2"; //失败终止
	public final static String DEPLOY_IP_KEY = "deploy_ip"; //部署ip
	public final static String PLAN_ID = "planId"; //部署计划id
	public final static String ENV = "env"; //部署计划对应的环境
	/**
	 * 客户端发出构建请求，jenkins开始构建，构建过程中把构建结果信息返回给前端
	 */

	/*
	 * source code:
	 * ebbc/ebcc
	 */
	public final static String SOURCE_CODE_KEY = "source_code";

	/*
	 * source server
	 * tfs: http://10.16.10.97:8080/tfs/defaultcollection
	 * svn: https://20.3.0.8/svn
	 */
	public final static String SOURCE_SERVER_KEY = "source_server";

	/*
	 * tfs: $/电子银行产品线/能力中心
	 * svn: 开发管控组/18自动化部署
	 */
	public final static String SOURCE_ROOT_KEY = "source_root";

	/*
	 * source parent: 
	 * REL_20170115/ebc-parent
	 */
	public final static String SOURCE_PARENT_KEY = "source_parent";

	/*
	 * 子项目目录名称:
	 * ebc-busi/ebc-busi-acc-service
	 */
	public final static String SOURCE_PATH_KEY = "source_path";

	/*
	 * 代码版本  key值
	 */
	public final static String SRC_REVISION_KEY = "src_revision";

	/*
	 * 生成包类型  key值
	 */
	public final static String PACKAGE_TYPE_KEY = "package_type";

	/*
	 * 生成包类型  valaue
	 * 0-zip包; 1-jar包; 2-war包
	 */
	public final static String PACKAGE_TYPE_0 = "0";
	public final static String PACKAGE_TYPE_1 = "1";
	public final static String PACKAGE_TYPE_2 = "2";

	/*
	 * 编译类型  key
	 */
	public final static String COMPILE_TYPE_KEY = "compile_type";
	/*
	 * 编译类型  valaue
	 * 0-完整包<full>, 1-程序包<libs>, 2-配置包<conf>
	 */
	public final static String COMPILE_TYPE_FULL = "0";
	public final static String COMPILE_TYPE_LIBS = "1";
	public final static String COMPILE_TYPE_CONF = "2";

	/*
	 * mvn编译命令类型  key
	 */
	public final static String MVN_COMP_COMM_KEY = "mvn_comm";
	/*
	 * mvn打包命令  valaue
	 * 0-install(默认), 1-deploy
	 */
	public final static String MVN_COMP_COMM__0 = "0";
	public final static String MVN_COMP_COMM__1 = "1";

	/**
	 * job类型：
	 * JOB_TYPE_EXPORT_SOURCE-导出源码JOB
	 * JOB_TYPE_COMPILE_APP-编译打包JOB
	 * JOB_TYPE_DEPLOY_APP-部署JOB
	 */
	public final static int JOB_TYPE_EXPORT_SOURCE = 0;
	public final static int JOB_TYPE_COMPILE_APP = 1;
	public final static int JOB_TYPE_DEPLOY_APP = 2;

	/**
	 *使用简单标准流程图作为 一对一或一对多部署流程ID
	 */
	public final static String FLOW_ID_SIMPLE_STANDARD_SUBFLOW = "SimpleStandardDeployFlow";



	/**
	 * 打包类型：
	 * PACK_TYPE_ALL-支持同时打包多个应用
	 * PACK_TYPE_ONE-打包单个应用
	 */
	public final static int PACK_TYPE_ALL = 0;
	public final static int PACK_TYPE_ONE = 1;
	/**
	 * 部署类型:
	 * DEPLOY_ALL-多对多部署
	 * DEPLOY_ONE-一对一部署
	 * DEPLOY_INST-一对多部署
	 * */

	public final static int DEPLOY_ALL = 0;
	public final static int DEPLOY_ONE = 1;
	public final static int DEPLOY_INST = 2;


	public final static String JOBS_MAP = "jobMap";
	public final static String JOBS_DEPLOY_LIST = "jobDeployList";
	public final static String JOBS_STOP_LIST = "jobStopList";
	public final static String JOBS_FILE_COPY_LIST = "jobFileCopyList";
	public final static String JOBS_START_OR_RESTART_LIST = "jobStartOrRestartList";
	public final static String SCRIPT_LIST = "scriptList";
	public final static String FLOW_ID = "flowId";
	public final static String SESSION_ID = "sessionId";
	public final static String ID = "id";
	public final static String RESULT = "result";
	public final static String APP_CODE = "appCode";
	public final static String LOA_BUSI = "ebc-busi-loa-service";
	public final static String CON_FLOW = "ebc-flow-con-service";

	public final static String FILE_COPY_BEFORE_SHUTDOWN = "copyBeforeShutdown";
	public final static String FILE_COPY_APP = "copyApp";
	public final static String FILE_COPY_JAR = "copyJar";
	public final static String FILE_COPY_WAR = "copyWar";
	public final static String FILE_COPY_CONF = "copyConf";

	public final static String CMD_RM = "rm";
	public final static String CMD_RM_RF = "rm -rf";

	public final static String REMOTE_COPY = "rcp";
	public final static String LOCAL_COPY = "lcp";

	public final static String JAR = ".jar";
	public final static String WAR = ".war";
	public final static String TAR = ".tar";
	public final static String ZIP = ".zip";

	public final static String FILE_INFO = "fileInfo";

	public final static String COPY_PARAM_NOZIP="0";
	public final static String COPY_PARAM_GZ="1";
	public final static String COPY_PARAM_ZIP="2";
	public final static String APP_TEMP_FILE_DIR="./appPack";

	public final static String TARGET_SHELL_DIR = "common";
	public final static String BOAR_DEPLOY_SCRIPT = "boar-deploy-script";

	public final static String AGENT_RECEIVE_FILES_ROOT_DIR="/home/app/agentFiles";

	//#源文件不压缩拷贝
	public final static String SOURCE_FILE_NOZIP="0";
	//源文件gz压缩
	public final static String SOURCE_FILE_GZ="1";
	//源文件zip压缩
	public final static String SOURCE_FILE_ZIP="2";
	//拷贝zip包并在远程服务器解压缩
	public final static String SOURCE_FILE_TRANS_AND_UNZIP="3";

	public final static String SHELL_EXEC = "exec.sh";

	public final static String SHELL_DEPLOY="deploy.sh";

	public final static String RE_TYPE = "rep_type";

	public final static String ZIP_PATH = "zip_path";

	public final static String APP_PATH = "app_path";

	public final static String SBIN_PATH = "sbin_path";
	//应用zip包
	public final static String RE_TYPE_ZIP = "0";
	//应用bin包
	public final static String RE_TYPE_FILE = "1";
	//应用lib包
	public final static String RE_TYPE_LIB = "2";

	public final static String VCS_SVN = "svn";

	public final static String VCS_TFS = "tfs";
	// 远程拷贝方向：1 - 远程拷贝至本地; else - 本地拷贝至远程
	public final static String RCP_FROM_REMOTE = "1";
	// 打包日志目录名
	public final static String DIR_BUILD = "pack";
	// 部署日志文件名
	public final static String DIR_DEPLOY = "deploy";
	// 前台展示日志的根目录名
	public final static String DIR_LOGS = "logs";

}
