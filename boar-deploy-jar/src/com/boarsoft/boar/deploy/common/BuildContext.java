package com.boarsoft.boar.deploy.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class BuildContext {
	/** 保存 buildId 和 websocket sessionId之间的映射关系 */
	private static Map<String, String> sessionMap = new ConcurrentHashMap<String, String>();
	/** build 实例 */
	private static Map<String, Object> buildMap = new ConcurrentHashMap<String, Object>();

	public static String getSessionId(String buildId) {
		return sessionMap.get(buildId);
	}

	public static boolean putSessionId(String buildId, String sessionId) {
		if (sessionMap.containsKey(buildId)) {
			return false; // 上一个构建或部署还未完成
		}
		sessionMap.put(buildId, sessionId);
		return true;
	}

	public static Object getBuild(String buildId) {
		return buildMap.get(buildId);
	}

	public static boolean putBuild(String buildId, Object flow) {
		if (buildMap.containsKey(buildId)) {
			return false; // 上一个构建或部署还未完成
		}
		buildMap.put(buildId, flow);
		return true;
	}
}