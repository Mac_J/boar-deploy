package com.boarsoft.boar.deploy.record;

import java.util.List;

import com.boarsoft.boar.deploy.entity.DeployRecord;
import com.boarsoft.common.dao.PagedResult;

public interface DeployRecordBiz {
	/**
	 * 查询部署记录列表
	 * 
	 * @param id
	 * @param planId
	 * @param detailId
	 * @param targetId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<DeployRecord> list(String id, String parentId, String planId, String detailId, String targetId, String serverId, String key, String env,
	                               String orderBy, int pageNo, int pageSize);
	
	/**
	 * 查询某个时间段之间的部署记录列表
	 * 
	 * @param startTime
	 * @param endTime
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<DeployRecord> listBetweenTime(String startTime, String endTime, String key, String env,
	                                          String orderBy, int pageNo, int pageSize);
	
	/**
	 * 保存一条部署记录信息，无需挂在树上
	 * @param a
	 */
	void save(DeployRecord a);

	/**
	 * 删除一条部署记录信息
	 * 
	 * @param id
	 */
	void delete(String id);

	/**
	 * 通过id获取一条部署记录信息
	 * 
	 * @param id
	 * @return
	 */
	DeployRecord get(String id);
	/**
	 * 通过parentId获取一条部署记录信息
	 * 
	 * @param idLike
	 * @param parentId
	 *
	 * @return
	 */
	List<DeployRecord> getByPatentIdAndIdLike(String idLike, String parentId);
}
