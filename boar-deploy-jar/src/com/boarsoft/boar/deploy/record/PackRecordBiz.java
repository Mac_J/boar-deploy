package com.boarsoft.boar.deploy.record;

import java.util.List;

import com.boarsoft.boar.deploy.entity.PackRecord;
import com.boarsoft.common.dao.PagedResult;

public interface PackRecordBiz {
	/**
	 * 查询打包记录列表
	 * 
	 * @param id
	 * @param planId
	 * @param detailId
	 * @param targetId
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<PackRecord> list(String id, String parentId, String planId, String detailId, String targetId, String env, String key,
	                             String orderBy, int pageNo, int pageSize);
	
	/**
	 * 查询某个时间段之间的打包记录列表
	 * 
	 * @param startTime
	 * @param endTime
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	PagedResult<PackRecord> listBetweenTime(String startTime, String endTime, String key, String env,
	                                        String orderBy, int pageNo, int pageSize);
	/**
	 * 查询一个plan的某个应用最新的打包记录
	 * 
	 * @param planId
	 * @param detailId
	 * @param targetId
	 * @param env
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	List<PackRecord> listLatestPackRecordInfo(String planId, String detailId, String targetId, String env, String key,
	                                          String orderBy, int pageNo, int pageSize);
	/**
	 * 保存一条打包记录信息，无需挂在树上
	 * @param a
	 */
	void save(PackRecord a);

	/**
	 * 删除一条打包记录信息
	 * 
	 * @param id
	 */
	void delete(String id);

	/**
	 * 通过id获取一条打包记录信息
	 * 
	 * @param id
	 * @return
	 */
	PackRecord get(String id);

	
}
