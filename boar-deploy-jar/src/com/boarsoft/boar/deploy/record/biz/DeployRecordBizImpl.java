package com.boarsoft.boar.deploy.record.biz;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.boarsoft.boar.deploy.entity.DeployRecord;
import com.boarsoft.boar.deploy.record.DeployRecordBiz;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.hibernate.biz.SimpleBizImpl;

@Component("deployRecordBiz")
public class DeployRecordBizImpl extends SimpleBizImpl implements DeployRecordBiz {

	@Override
	@Transactional(readOnly = true)
	public PagedResult<DeployRecord> list(String id, String parentId, String planId, String detailId, String targetId, String serverId,
			String key,String env, String orderBy, int pageNo, int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(DeployRecord.class.getName());
		sb.append(" where 1=1");
		if (Util.strIsNotEmpty(id)) {
			sb.append(" and id='").append(id).append("'");
		}
		if (Util.strIsNotEmpty(parentId)) {
			sb.append(" and parentId='").append(parentId).append("'");
		}
		if (Util.strIsNotEmpty(planId)) {
			sb.append(" and planId='").append(planId).append("'");
		}
		if (Util.strIsNotEmpty(detailId)) {
			sb.append(" and detailId='").append(detailId).append("'");
		}
		if (Util.strIsNotEmpty(targetId)) {
			sb.append(" and targetId='").append(targetId).append("'");
		}
		if (Util.strIsNotEmpty(serverId)) {
			sb.append(" and serverId='").append(serverId).append("'");
		}
		if (Util.strIsNotEmpty(env)) {
			sb.append(" and env='").append(env).append("'");
		}
		sb.append(" and targetId is not null");
		if (Util.strIsNotEmpty(key)) {
			sb.append(" and (env like '%").append(key);
			sb.append("%' or version like '%").append(key);
			sb.append("%' or deployPath like '%").append(key);
			sb.append("%')");
		}
		String ql = sb.toString();
		int total = dao.getTotal("select count(id)".concat(ql));
		if (Util.strIsNotEmpty(orderBy)) {
			sb.append(" order by ").append(orderBy);
		} else {
			sb.append(" order by id");
		}
		List<DeployRecord> lt = dao.list(sb.toString(), pageNo, pageSize);
		return new PagedResult<DeployRecord>(total, lt, pageNo, pageSize);
	}

	@Override
	@Transactional(readOnly = true)
	public PagedResult<DeployRecord> listBetweenTime(String startTime, String endTime, String key, String env,String orderBy, int pageNo, int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(DeployRecord.class.getName());
		sb.append(" where 1=1");
		if (Util.strIsNotEmpty(startTime)) {
			sb.append(" and startTime >= '").append(startTime).append("'");
		}
		if (Util.strIsNotEmpty(endTime)) {
			sb.append(" and endTime <= '").append(endTime).append("'");
		}
		if (Util.strIsNotEmpty(env)) {
			sb.append(" and env = '").append(env).append("'");
		}
		if (Util.strIsNotEmpty(key)) {
			sb.append(" and (env like '%").append(key);
			sb.append("%' or version like '%").append(key);
			sb.append("%' or deployPath like '%").append(key);
			sb.append("%')");
		}
		String ql = sb.toString();
		int total = dao.getTotal("select count(id)".concat(ql));
		if (Util.strIsNotEmpty(orderBy)) {
			sb.append(" order by ").append(orderBy);
		} else {
			sb.append(" order by id");
		}
		List<DeployRecord> lt = dao.list(sb.toString(), pageNo, pageSize);
		return new PagedResult<DeployRecord>(total, lt, pageNo, pageSize);
	}

	@Override
	@Transactional
	public void save(DeployRecord o) {
		if (Util.strIsEmpty(o.getId())) {
			dao.save(o);
		} else {
			dao.merge(o);
		}
	}

	@Override
	@Transactional
	public void delete(String id) {
		dao.delete(DeployRecord.class, id);
	}

	@Override
	@Transactional(readOnly = true)
	public DeployRecord get(String id) {
		return (DeployRecord) dao.get(DeployRecord.class, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<DeployRecord> getByPatentIdAndIdLike(String idLike, String parentId) {
		StringBuilder sb = new StringBuilder();
		sb.append("select d from ").append(DeployRecord.class.getName()).append(" d where 1=1 ");
		
		if (Util.strIsNotEmpty(parentId)) {
			sb.append(" and d.parentId='").append(parentId).append("'");
		}
		if (Util.strIsNotEmpty(idLike)) {
			sb.append(" and and d.id like '%").append(idLike).append("%'");
		}
		
		List<DeployRecord> list = dao.list(sb.toString());
		return list;
	}

}
