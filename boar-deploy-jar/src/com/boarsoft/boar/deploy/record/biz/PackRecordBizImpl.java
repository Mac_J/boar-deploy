package com.boarsoft.boar.deploy.record.biz;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.boarsoft.boar.deploy.entity.BuildPlanItem;
import com.boarsoft.boar.deploy.entity.PackRecord;
import com.boarsoft.boar.deploy.record.PackRecordBiz;
import com.boarsoft.common.Util;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.hibernate.biz.SimpleBizImpl;

@Component("packRecordBiz")
public class PackRecordBizImpl extends SimpleBizImpl implements PackRecordBiz {

	@Override
	@Transactional(readOnly = true)
	public PagedResult<PackRecord> list(String id, String parentId, String planId, String detailId, String targetId, String key,String env,
			String orderBy, int pageNo, int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(PackRecord.class.getName());
		sb.append(" where 1=1");
		if (Util.strIsNotEmpty(id)) {
			sb.append(" and id='").append(id).append("'");
		}
		if (Util.strIsNotEmpty(parentId)) {
			sb.append(" and parentId='").append(parentId).append("'");
		}
		if (Util.strIsNotEmpty(planId)) {
			sb.append(" and planId='").append(planId).append("'");
		}
		if (Util.strIsNotEmpty(detailId)) {
			sb.append(" and detailId='").append(detailId).append("'");
		}
		if (Util.strIsNotEmpty(targetId)) {
			sb.append(" and targetId='").append(targetId).append("'");
		}
		if (Util.strIsNotEmpty(env)) {
			sb.append(" and env='").append(env).append("'");
		}
		sb.append(" and targetId is not null");
		if (Util.strIsNotEmpty(key)) {
			sb.append(" and (env like '%").append(key);
			sb.append("%' or version like '%").append(key);
			sb.append("%' or packPath like '%").append(key);
			sb.append("%')");
		}
		String ql = sb.toString();
		int total = dao.getTotal("select count(id)".concat(ql));
		if (Util.strIsNotEmpty(orderBy)) {
			sb.append(" order by ").append(orderBy);
		} else {
			sb.append(" order by id");
		}
		List<PackRecord> lt = dao.list(sb.toString(), pageNo, pageSize);
		return new PagedResult<PackRecord>(total, lt, pageNo, pageSize);
	}

	@Override
	@Transactional(readOnly = true)
	public PagedResult<PackRecord> listBetweenTime(String startTime, String endTime, String key, String env,String orderBy, int pageNo, int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append(" from ").append(PackRecord.class.getName());
		sb.append(" where 1=1");
		if (Util.strIsNotEmpty(startTime)) {
			sb.append(" and startTime >= '").append(startTime).append("'");
		}
		if (Util.strIsNotEmpty(endTime)) {
			sb.append(" and endTime <= '").append(endTime).append("'");
		}
		if (Util.strIsNotEmpty(env)) {
			sb.append(" and env = '").append(env).append("'");
		}

		if (Util.strIsNotEmpty(key)) {
			sb.append("and (env like '%").append(key);
			sb.append("%' or version like '%").append(key);
			sb.append("%' or packPath like '%").append(key);
			sb.append("%')");
		}
		String ql = sb.toString();
		int total = dao.getTotal("select count(id)".concat(ql));
		if (Util.strIsNotEmpty(orderBy)) {
			sb.append(" order by ").append(orderBy);
		} else {
			sb.append(" order by id");
		}
		List<PackRecord> lt = dao.list(sb.toString(), pageNo, pageSize);
		return new PagedResult<PackRecord>(total, lt, pageNo, pageSize);
	}

	@Override
	@Transactional
	public void save(PackRecord o) {
		if (Util.strIsEmpty(o.getId())) {
			dao.save(o);
		} else {
			dao.merge(o);
		}
	}

	@Override
	@Transactional
	public void delete(String id) {
		dao.delete(PackRecord.class, id);
	}

	@Override
	@Transactional(readOnly = true)
	public PackRecord get(String id) {
		return (PackRecord) dao.get(PackRecord.class, id);
	}

	/**
	 * 查询一个plan的某个应用最新的打包记录
	 * 
	 * @param planId
	 * @param detailId
	 * @param targetId
	 * @param env
	 * @param key
	 * @param orderBy
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	@Override
	@Transactional(readOnly = true)
	public List<PackRecord> listLatestPackRecordInfo(String planId, String detailId, String targetId, String env, String key,
			String orderBy, int pageNo, int pageSize) {
		StringBuilder sb = new StringBuilder();
		sb.append("select new PackRecord(pR.status,pR.targetId,pR.packPath,max(pR.lastUpdateTime)) from ").append(
				PackRecord.class.getName()).append(" pR where 1=1").append(" and pR.planId='").append(planId).append("'");
		if (Util.strIsNotEmpty(detailId)) {
			sb.append(" and pR.detailId='").append(detailId).append("'");
		}
		if (Util.strIsNotEmpty(targetId)) {
			sb.append(" and pR.targetId='").append(targetId).append("'");
		}
		if (Util.strIsNotEmpty(env)) {
			sb.append(" and pR.env='").append(env).append("'");
		}

		if (Util.strIsNotEmpty(key)) {
			sb.append("and (pR.env like '%").append(key);
			sb.append("%' or pR.version like '%").append(key);
			sb.append("%' or pR.packPath like '%").append(key);
			sb.append("%')");
		}

		sb.append(" and pR.targetId in (select b.targetId from ").append(BuildPlanItem.class.getName())
//				.append(" b where b.planId=pR.planId) group by pR.targetId");
				.append(" b where b.planId=pR.planId)");

		
		if (Util.strIsNotEmpty(orderBy)) {
			sb.append(" order by pR.").append(orderBy);
		} else {
			sb.append(" order by pR.lastTime DESC");
		}
		List<PackRecord> lt = dao.list(sb.toString(), pageNo, pageSize);
		
		return lt;
	}

}
