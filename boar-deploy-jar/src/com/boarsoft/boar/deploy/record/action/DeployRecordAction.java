package com.boarsoft.boar.deploy.record.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.DeployRecord;
import com.boarsoft.boar.deploy.record.DeployRecordBiz;
import com.boarsoft.common.Authorized;
import com.boarsoft.common.dao.PagedResult;
import com.boarsoft.common.util.JsonUtil;

@RestController
@RequestMapping("/record/deploy")
public class DeployRecordAction {
	// private static final Logger log =
	// LoggerFactory.getLogger(DeployRecordAction.class);

	@Autowired
	private DeployRecordBiz deployRecordBiz;

	/**
	 * 查询部署记录 支持使用id,planId,detailId,targetId查询 同时支持关键字过滤及按字段排序
	 */
	@RequestMapping("/list")
	@Authorized(code = "record.deploy.list")
	public ReplyInfo<Object> list(String id, String parentId, String planId, String detailId, String targetId, String serverId,
			String env, String key, String orderBy, int pageNo, int pageSize) {
		PagedResult<DeployRecord> pr = deployRecordBiz.list(id, parentId, planId, detailId, targetId, serverId, key, env,
				orderBy, pageNo, pageSize);
		return new ReplyInfo<Object>(true, JsonUtil.from(pr));
	}

	/**
	 * 在某个时间区间内查询部署记录 同时支持关键字过滤及按字段排序
	 */
	@RequestMapping("/listBetweenTime")
	@Authorized(code = "record.deploy.listBetweenTime")
	public ReplyInfo<Object> listBetweenTime(String startTime, String endTime, String env, String key, String orderBy,
			int pageNo, int pageSize) {
		PagedResult<DeployRecord> pr = deployRecordBiz.listBetweenTime(startTime, endTime, key, env, orderBy, pageNo, pageSize);
		return new ReplyInfo<Object>(true, JsonUtil.from(pr));
	}
}
