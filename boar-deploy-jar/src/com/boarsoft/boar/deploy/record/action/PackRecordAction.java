package com.boarsoft.boar.deploy.record.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.entity.PackRecord;
import com.boarsoft.boar.deploy.record.PackRecordBiz;
import com.boarsoft.common.Authorized;
import com.boarsoft.common.dao.PagedResult;

@RestController
@RequestMapping("/record/pack")
public class PackRecordAction {
	@Autowired
	private PackRecordBiz packRecordBiz;

	/**
	 * 查询打包记录 支持使用id,planId,detailId,targetId查询 同时支持关键字过滤及按字段排序
	 */
	@RequestMapping("/list.do")
	@Authorized(code = "record.pack.list")
	public ReplyInfo<Object> list(String id, String parentId, String planId, String detailId, String targetId, String key,
			String env, String orderBy, int pageNo, int pageSize) {
		PagedResult<PackRecord> pr = packRecordBiz.list(id, parentId, planId, detailId, targetId, key, env, orderBy, pageNo,
				pageSize);
		return new ReplyInfo<Object>(true, pr);
	}

	/**
	 * 在某个时间区间内查询打包记录 同时支持关键字过滤及按字段排序
	 */
	@RequestMapping("/between.do")
	@Authorized(code = "record.pack.between")
	public ReplyInfo<Object> between(String startTime, String endTime, String key, String env, String orderBy, int pageNo,
			int pageSize) {
		PagedResult<PackRecord> pr = packRecordBiz.listBetweenTime(startTime, endTime, key, env, orderBy, pageNo, pageSize);
		return new ReplyInfo<Object>(true, pr);
	}
}