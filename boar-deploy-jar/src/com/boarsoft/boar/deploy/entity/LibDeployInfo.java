package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

import com.boarsoft.boar.entity.LibInfo;

public class LibDeployInfo extends LibInfo implements Serializable {
	private static final long serialVersionUID = -2249330115246589969L;

	/** 源码导出来源code：default_tfs_export-tfs导出 default_svn_export-svn导出 */
	protected String exportCode;
	/** 打包构建码 此字段可为空，如果不配置值则表示此应用无需打包 */
	protected String compileCode;
	/** 部署构建码 */
	protected String deployCode;
	/** 应用源码的来源编码，用以区分源码的不同VCS来源 例如：ebbc-svn ebbc-tfs */
	protected String sourceCode;
	/** 应用源码的相对路径 */
	protected String sourcePath;
	/** 应用在服务器上的部署路径，默认为：/home/app/deploy */
	protected String deployPath = "/home/app/deploy";
	/** 包生成路径，例如：CIT/ebc-parent/0.0.1/ebc-busi-usr-service-0.0.1-SNAPSHOT-bin.zip */
	protected String packPath;

	protected String sourceParent;

	public String getExportCode() {
		return exportCode;
	}

	public void setExportCode(String exportCode) {
		this.exportCode = exportCode;
	}

	public String getCompileCode() {
		return compileCode;
	}

	public void setCompileCode(String compileCode) {
		this.compileCode = compileCode;
	}

	public String getDeployCode() {
		return deployCode;
	}

	public void setDeployCode(String deployCode) {
		this.deployCode = deployCode;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getDeployPath() {
		return deployPath;
	}

	public void setDeployPath(String deployPath) {
		this.deployPath = deployPath;
	}

	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

	public String getSourceParent() {
		return sourceParent;
	}

	public void setSourceParent(String sourceParent) {
		this.sourceParent = sourceParent;
	}
}