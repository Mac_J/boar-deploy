package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

public class JobInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3389635934341883052L;
	/**
	 * job类型
	 */
	public static final String JOB_TYPE_EXPORT = "0";
	public static final String JOB_TYPE_COMPILE = "1";
	public static final String JOB_TYPE_FILE_COPY = "2";
	public static final String JOB_TYPE_SHUTDOWN_SERVER = "3";
	public static final String JOB_TYPE_START_SERVER = "4";
	public static final String JOB_TYPE_RESTART_SERVER = "5";
	public static final String JOB_TYPE_DEPLOY = "6";
	/**
	 * job执行状态
	 */
	public static final short JOB_EXEC_STATUS_INITIAL = 0;
	public static final short JOB_EXEC_STATUS_BUILDLING = 1;
	public static final short JOB_EXEC_STATUS_SUCCESS = 2;
	public static final short JOB_EXEC_STATUS_FAILED = 3;

	/**job可用状态*/
	public static final short JOB_ENABLE_NO=0;
	public static final short JOB_ENABLE_YES=1;

	/**job对象类型*/
	public static final String JOB_TARGET_TYPE_APP = "0";
	public static final String JOB_TARGET_TYPE_JAR = "1";
	public static final String JOB_TARGET_TYPE_WAR = "2";
	public static final String JOB_TARGET_TYPE_CONF = "3";
	public static final String JOB_TARGET_TYPE_SERVER = "4";
	public static final String JOB_TARGET_TYPE_FILE_BEFORE_SHUTDOWN = "5";

	/**job 部署类型*/
	public static final String JOB_DEPLOY_TYPE_APP = "0";
	public static final String JOB_DEPLOY_TYPE_LIB = "1";
	public static final String JOB_DEPLOY_TYPE_BIN = "2";
	public static final String JOB_DEPLOY_TYPE_SHUTDOWN = "3";
	public static final String JOB_DEPLOY_TYPE_START = "4";
	public static final String JOB_DEPLOY_TYPE_RESTART = "5";

	/**job 构建策略*/
	//0 -- 顺序部署 1 -- 半数并发 2 -- 全量并发
	public static final String JOB_BUILD_POLICY_ONEBYONE = "0";
	public static final String JOB_BUILD_POLICY_HALFBYHALF = "1";
	public static final String JOB_BUILD_POLICY_ALL = "2";
	/**
	 * 主键 ID
	 */
	private String id;
	/**
	 * planId
	 */
	private String planId;


	/**
	 * job类型: 0-导源码，1-编译打包，2-文件拷贝，3-停机，4-启动，5-重启，6-部署
	 */
	private String jobType;
	/**
	 * 同一plan下同一类型job类型的优先级：数字越小优先级越高
	 */
	private short jobPriority;
	/**
	 * job构建码：来源于exportCode或compileCode或deployCode或shutdownCode或startCode或restartCode
	 */
	private String jobBuildCode;
	/**
	 * job构建详细分类：
	 * 打包时对应packType(compType):0-完整包,1-程序包,2-配置包;
	 * 部署时对应deployType：0-部署单个应用包, 1-部署单个应用包中的lib包, 2-部署单个应用包中的bin包，3-停单个应用,4-启单个应用,5-重启单个应用
	 */
	private String jobBuildType;
	/**
	 * 部署时构建策略(deployPolicy)： 0-顺序构建 1-半数构建 2-全量并发构建
	 */
	private String jobBuildPolicy;
	/**
	 * job构建用户
	 */
	private String jobBuildUsr;
	/**
	 * job构建用户密码
	 */
	private String jobBuildPwd;
	/**
	 * job构建环境
	 */
	private String jobEnv;
	/**
	 * job回调url地址
	 */
	private String jobCallBackURL;
	/**
	 * job结果类型：0-zip包; 1-jar包; 2-war包
	 */
	private String jobResultType;
	/**
	 * jobParentId
	 */
	private String jobParentId;
	/**
	 * job状态：0-初始化; 1-执行中; 2-执行成功; 3-执行失败
	 */
	private short jobExecStatus;
	/**
	 * job可用状态：0-不可用（表示job已取消）; 1-可用（表示job待执行）
	 */
	private short jobEnable;
	/**
	 * 应用Id
	 * */
	private String appId;
	/**
	 * libId
	 * */
	private String libId;
	/**
	 * 文件Id
	 * */
	private String fileId;
	/**
	 * 服务器Id
	 * */
	private String serverId;
	/**
	 * 服务部署端口
	 * */
	private String servicePort;
	/**
	 * job对象类型：0-应用，1-jar包，2-war包，3-配置文件，4-服务器，5-停机前文件拷贝
	 */
	private String targetType;
	/**
	 * job对象源码的来源服务器
	 */
	private String targetSourceServer;
	/**
	 * job对象源码的来源服务器上的分支路径，为相对于vTargetSourceRoot的路径
	 */
	private String targetSourceRoot;
	/**
	 * job对象源码工程路径（目标的上一级路径，即父路径）,为相对于vTargetSourceRoot的路径
	 */
	private String targetSourceParent;
	/**
	 * job对象源码路径（目标的真实路径）,为相对于vTargetSourceParent的路径，此路径在工程下只有一个目标对象时，可配置为：/
	 */
	private String targetSourcePath;
	/**
	 *
	 * */
	private String targetShellPath;
	/**
	 * job对象源码来源编码
	 */
	private String targetSourceCode;
	/**
	 * job对象编码
	 */
	private String targetCode;
	/**
	 * job对象名
	 */
	private String targetName;
	/**
	 * job对象版本
	 */
	private String targetVersion;
	/**
	 * job对象源位置1
	 */
	private String targetFrom1;
	/**
	 * job对象源位置2
	 */
	private String targetFrom2;
	/**
	 * job对象目标位置(一般为临时目录位置)
	 */
	private String targetTo0;
	/**
	 * job对象目标位置
	 */
	private String targetTo1;
	/**
	 * job对象目标位置
	 */
	private String targetTo2;


	/**
	 * job创建时间
	 */
	private String createTime;
	/**
	 * job最后更新时间
	 */
	private String lastUpdateTime;
	/**
	 * 备注，job描述
	 */
	private String memo;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public short getJobPriority() {
		return jobPriority;
	}

	public void setJobPriority(short jobPriority) {
		this.jobPriority = jobPriority;
	}

	public String getJobBuildCode() {
		return jobBuildCode;
	}

	public void setJobBuildCode(String jobBuildCode) {
		this.jobBuildCode = jobBuildCode;
	}

	public String getJobBuildType() {
		return jobBuildType;
	}

	public void setJobBuildType(String jobBuildType) {
		this.jobBuildType = jobBuildType;
	}

	public String getJobBuildPolicy() {
		return jobBuildPolicy;
	}

	public void setJobBuildPolicy(String jobBuildPolicy) {
		this.jobBuildPolicy = jobBuildPolicy;
	}

	public String getJobBuildUsr() {
		return jobBuildUsr;
	}

	public void setJobBuildUsr(String jobBuildUsr) {
		this.jobBuildUsr = jobBuildUsr;
	}

	public String getJobBuildPwd() {
		return jobBuildPwd;
	}

	public void setJobBuildPwd(String jobBuildPwd) {
		this.jobBuildPwd = jobBuildPwd;
	}

	public String getJobEnv() {
		return jobEnv;
	}

	public void setJobEnv(String jobEnv) {
		this.jobEnv = jobEnv;
	}

	public String getJobCallBackURL() {
		return jobCallBackURL;
	}

	public void setJobCallBackURL(String jobCallBackURL) {
		this.jobCallBackURL = jobCallBackURL;
	}

	public String getJobResultType() {
		return jobResultType;
	}

	public void setJobResultType(String jobResultType) {
		this.jobResultType = jobResultType;
	}

	public String getJobParentId() {
		return jobParentId;
	}

	public void setJobParentId(String jobParentId) {
		this.jobParentId = jobParentId;
	}

	public short getJobExecStatus() {
		return jobExecStatus;
	}

	public void setJobExecStatus(short jobExecStatus) {
		this.jobExecStatus = jobExecStatus;
	}

	public short getJobEnable() {
		return jobEnable;
	}

	public void setJobEnable(short jobEnable) {
		this.jobEnable = jobEnable;
	}

	public String getServicePort() {
		return servicePort;
	}

	public void setServicePort(String servicePort) {
		this.servicePort = servicePort;
	}

	public String getTargetType() {
		return targetType;
	}

	public void setTargetType(String targetType) {
		this.targetType = targetType;
	}

	public String getTargetSourceCode() {
		return targetSourceCode;
	}

	public void setTargetSourceCode(String targetSourceCode) {
		this.targetSourceCode = targetSourceCode;
	}

	public String getTargetCode() {
		return targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getTargetVersion() {
		return targetVersion;
	}

	public void setTargetVersion(String targetVersion) {
		this.targetVersion = targetVersion;
	}

	public String getTargetFrom1() {
		return targetFrom1;
	}

	public void setTargetFrom1(String targetFrom1) {
		this.targetFrom1 = targetFrom1;
	}

	public String getTargetFrom2() {
		return targetFrom2;
	}

	public void setTargetFrom2(String targetFrom2) {
		this.targetFrom2 = targetFrom2;
	}

	public String getTargetTo0() {
		return targetTo0;
	}

	public void setTargetTo0(String targetTo0) {
		this.targetTo0 = targetTo0;
	}

	public String getTargetTo1() {
		return targetTo1;
	}

	public void setTargetTo1(String targetTo1) {
		this.targetTo1 = targetTo1;
	}

	public String getTargetTo2() {
		return targetTo2;
	}

	public void setTargetTo2(String targetTo2) {
		this.targetTo2 = targetTo2;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getTargetSourceServer() {
		return targetSourceServer;
	}

	public void setTargetSourceServer(String targetSourceServer) {
		this.targetSourceServer = targetSourceServer;
	}

	public String getTargetSourceRoot() {
		return targetSourceRoot;
	}

	public void setTargetSourceRoot(String targetSourceRoot) {
		this.targetSourceRoot = targetSourceRoot;
	}

	public String getTargetSourceParent() {
		return targetSourceParent;
	}

	public void setTargetSourceParent(String targetSourceParent) {
		this.targetSourceParent = targetSourceParent;
	}

	public String getTargetSourcePath() {
		return targetSourcePath;
	}

	public void setTargetSourcePath(String targetSourcePath) {
		this.targetSourcePath = targetSourcePath;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getLibId() {
		return libId;
	}

	public void setLibId(String libId) {
		this.libId = libId;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getTargetShellPath() {
		return targetShellPath;
	}

	public void setTargetShellPath(String targetShellPath) {
		this.targetShellPath = targetShellPath;
	}
}
