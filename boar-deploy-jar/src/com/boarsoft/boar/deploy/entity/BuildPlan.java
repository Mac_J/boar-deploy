package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

/**
 * 部署计划
 * 
 * @author Mac_J
 *
 */
public class BuildPlan implements Serializable {
	private static final long serialVersionUID = 2672406554877841703L;
	/** 已关闭/未使用 */
	public static final short STATUS_NOUSE = 0;
	/** 草稿 */
	public static final short STATUS_DRAFT = 1;
	/** 已初始化 */
	public static final short STATUS_READY = 2;

	/** 计划ID */
	private String id;
	/** 计划编号 */
	private String code;
	/** 工程ID */
	private String projId;
	/** 打包构建码 */
	private String packCode;
	/** 部署流程ID或Jenkins的任务名 */
	private String deployCode;
	/** 计划名称 */
	private String name;
	/** 计划状态：0：已关闭使用，1：草稿，2：已初始化 */
	private short status = STATUS_DRAFT;
	/** 部署应用计划上线时间，用于定时发布 */
	private String onlineTime;
	/** 备注 */
	private String memo;
	/** 所属系统 */
	private String catalog;
	/** 打包服务器地址 */
	private String packServer;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProjId() {
		return projId;
	}

	public void setProjId(String projId) {
		this.projId = projId;
	}

	public String getPackCode() {
		return packCode;
	}

	public void setPackCode(String packCode) {
		this.packCode = packCode;
	}

	public String getDeployCode() {
		return deployCode;
	}

	public void setDeployCode(String deployCode) {
		this.deployCode = deployCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}

	public String getOnlineTime() {
		return onlineTime;
	}

	public void setOnlineTime(String onlineTime) {
		this.onlineTime = onlineTime;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getPackServer() {
		return packServer;
	}

	public void setPackServer(String packServer) {
		this.packServer = packServer;
	}
}