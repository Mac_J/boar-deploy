package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

import com.boarsoft.boar.entity.FileInfo;

public class FileDeployInfo extends FileInfo implements Serializable {
	private static final long serialVersionUID = 4114452477863077426L;

	/** 配置文件源文件在svn上的路径 */
	protected String sourcePath;
	/** 包生成路径，例如：ebc-parent/0.0.1/ebc-busi-usr-service-0.0.1-SNAPSHOT-bin.zip */
	protected String packPath;

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

}
