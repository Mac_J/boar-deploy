package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

import com.boarsoft.boar.entity.AppInfo;
import com.boarsoft.common.Util;

/**
 * 
 * 
 * @author Mac_J
 *
 */
public class AppDeployInfo extends AppInfo implements Serializable {
	private static final long serialVersionUID = 1250040289364164550L;

	/** 应用分类：0：普通应用（需要导源码、打包编译、部署） */
	public static final short APP_TYPE_NORMAL = 0;
	/** 应用分类：1：属于配置一类的应用（只需导源码，无需打包编译、部署、停机、启动、重启等动作） */
	public static final short APP_TYPE_SPECIAL_1 = 1;
	/** 应用分类：2：属于配置一类的应用（只需导源码，可能需打包编译、部署，但无停机启动重启动作） */
	public static final short APP_TYPE_SPECIAL_2 = 2;

	/** 应用打包和部署时的优先级，数字越大优先级越高 */
	protected short level = 1;
	/** 源码导出脚本路径、流程ID或Jenkins任务名 */
	protected String exportCode;
	/** 打包构建码 此字段可为空，如果不配置值则表示此应用无需打包 */
	protected String compileCode;
	/** 部署构建码 */
	protected String deployCode;
	/** 卸载码 */
	protected String undeployCode;
	/**
	 * 应用源码的相对路径<br>
	 * 注：源码URL的上半部份位于exportCode对应的脚本文件或JENKINS配置中
	 */
	protected String sourcePath;
	/** 包生成路径，例如：CIT/ebc-parent/0.0.1/ebc-busi-usr-service-0.0.1-SNAPSHOT-bin.zip */
	protected String packPath;

	/**
	 * 返回替换了变量的打包文件路径
	 * 
	 * @return
	 */
	public String getPackPath2() {
		if (Util.strIsEmpty(packPath)) {
			return packPath;
		}
		return packPath.replaceAll("\\$\\{code\\}", code)//
				.replaceAll("\\$\\{version\\}", ver);
	}

	public String getDeployPath2() {
		if (Util.strIsEmpty(deployPath)) {
			return deployPath;
		}
		return deployPath.replaceAll("\\$\\{code\\}", code)//
				.replaceAll("\\$\\{version\\}", ver);
	}

	public short getLevel() {
		return level;
	}

	public void setLevel(short level) {
		this.level = level;
	}

	public String getExportCode() {
		return exportCode;
	}

	public void setExportCode(String exportCode) {
		this.exportCode = exportCode;
	}

	public String getCompileCode() {
		return compileCode;
	}

	public void setCompileCode(String compileCode) {
		this.compileCode = compileCode;
	}

	public String getDeployCode() {
		return deployCode;
	}

	public void setDeployCode(String deployCode) {
		this.deployCode = deployCode;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	/**
	 * 返回原始的（含变量的）打包文件路径
	 * 
	 * @return
	 */
	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

	public String getUndeployCode() {
		return undeployCode;
	}

	public void setUndeployCode(String undeployCode) {
		this.undeployCode = undeployCode;
	}
}