package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

/** 编译打包记录Po */
public class PackRecord implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5571401452556188484L;
	/** 主键id */
	private String id;
	/** 父构建Id */
	private String parentId;
	/** planId */
	private String planId;
	/** 所属打包详情Id */
	private String detailId;
	/** 要处理的对象ID（应用或JAR包）————冗余自plan_detail */
	private String targetId;
	/** code，冗余自app_info表code字段 */
	private String targetCode;
	/** 编译job开始时间 */
	private String startTime;
	/** 编译job结束时间 */
	private String endTime;
	/** 记录最后更新时间 */
	private String lastUpdateTime;
	/** 打包环境 */
	private String env;
	/** 打包版本 */
	private String ver;
	/** 优先级（打包或部署顺序） */
	private String targetLevel;
	/** 打包结果类型 0-zip 1-jar 2-war等————目前前端没有设计让用户选择，而是在代码里写死为0 */
	private String resultType;
	/** 编译类型 0-编译完整包,1-编译程序包,2-编译配置包 */
	private String compileType;
	/** 执行阶段 0-初始化，1-下载源码，2-编译打包 */
	private String stage;
	/** 执行状态 0-成功 1-失败退出 2-失败继续 */
	private String status;
	/** 包存放路径 */
	private String packPath;

	public PackRecord() {

	}

	//
	public PackRecord(String status, String targetId, String packPath,
			String lastUpdateTime) {
		this.status = status;
		this.packPath = packPath;
		this.lastUpdateTime = lastUpdateTime;
		this.targetId = targetId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public String getCompileType() {
		return compileType;
	}

	public void setCompileType(String compileType) {
		this.compileType = compileType;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

	public String getTargetCode() {
		return targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

}
