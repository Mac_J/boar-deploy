package com.boarsoft.boar.deploy.entity;

/**
 * Jenkins回调事件
 */
public class EventInfo {
	public static final String CON_STATE_STARTING = "0";
	public static final String CON_STATE_CONSTRUCTING = "1";
	public static final String CON_STATE_COMPLETE_SUCCESS = "2";
	public static final String CON_STATE_COMPLETE_ERROR = "2";
	/**
	 * 本次任务的总conId
	 */
	private String conId;
	/**
	 * conId根据服务器列表数量被Jenkins拆分后的子构建id
	 * 拆分规则：有多少个节点就被拆分成多少个子id
	 * */
	private String subConId;
	/**
	 * 构建状态
	 */
	private String conState;
	/**
	 * 刚构建【打包或部署】完成的服务器IP
	 */
	private String conIp;

	public String getConId() {
		return conId;
	}

	public void setConId(String conId) {
		this.conId = conId;
	}

	public String getSubConId() {
		return subConId;
	}

	public void setSubConId(String subConId) {
		this.subConId = subConId;
	}

	public String getConState() {
		return conState;
	}

	public void setConState(String conState) {
		this.conState = conState;
	}

	public String getConIp() {
		return conIp;
	}

	public void setConIp(String conIp) {
		this.conIp = conIp;
	}

}
