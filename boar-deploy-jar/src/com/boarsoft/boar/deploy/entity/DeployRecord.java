package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

/** 部署记录Po */
public class DeployRecord implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1716147733739382204L;
	/** 主键id */
	private String id;
	/**
	 * 操作类型，对应前端在部署时点击的三种不同部署方式
	 * 0-deployMany2Many
	 * 1-deployOneApp2Servers
	 * 1-deployOneApp2OneServer
	 * */
	private String opType;
	/** 父构建Id */
	private String parentId;
	/** planId */
	private String planId;
	/** 所属打包详情Id */
	private String detailId;
	/** 要处理的对象ID（应用或JAR包）————冗余自plan_detail */
	private String targetId;
	/** code，冗余自app_info表code字段 */
	private String targetCode;
	/** 服务器id */
	private String serverId;
	/** 编译job开始时间 */
	private String startTime;
	/** 编译job结束时间 */
	private String endTime;
	/** 记录最后更新时间 */
	private String lastUpdateTime;
	/** 打包环境 */
	private String env;
	/** 打包版本 */
	private String ver;
	/** Ip列表 */
	private String ipList;
	/** 优先级（打包或部署顺序） */
	private String targetLevel;
	/**
	 * deployType：0-部署单个应用包 1-部署单个应用包中的lib包 2-部署单个应用包中的bin包 对应
	 * packType：0-完整包,1-程序包,2-配置包
	 */
	private String deployType;
	/** 构建策略：0-顺序部署，1-半数并发，2-全量并发 */
	private String deployPolicy;
	/** 执行阶段 0-初始化，1-传输jar包或配置文件，2-部署应用程序 */
	private String stage;
	/** 执行状态 0-成功 1-失败退出 2-失败继续 */
	private String status;
	/** 应用或库包在服务器上的相对部署路径 */
	private String deployPath;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getDetailId() {
		return detailId;
	}

	public void setDetailId(String detailId) {
		this.detailId = detailId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLastUpdateTime() {
		return lastUpdateTime;
	}

	public void setLastUpdateTime(String lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getTargetLevel() {
		return targetLevel;
	}

	public void setTargetLevel(String targetLevel) {
		this.targetLevel = targetLevel;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getIpList() {
		return ipList;
	}

	public void setIpList(String ipList) {
		this.ipList = ipList;
	}

	public String getDeployPolicy() {
		return deployPolicy;
	}

	public void setDeployPolicy(String deployPolicy) {
		this.deployPolicy = deployPolicy;
	}

	public String getDeployPath() {
		return deployPath;
	}

	public void setDeployPath(String deployPath) {
		this.deployPath = deployPath;
	}

	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}

	public String getOpType() {
		return opType;
	}

	public void setOpType(String opType) {
		this.opType = opType;
	}

	public String getDeployType() {
		return deployType;
	}

	public void setDeployType(String deployType) {
		this.deployType = deployType;
	}

	public String getTargetCode() {
		return targetCode;
	}

	public void setTargetCode(String targetCode) {
		this.targetCode = targetCode;
	}

}
