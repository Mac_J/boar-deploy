package com.boarsoft.boar.job;

public class BaseExportJob extends BuildJob implements IExportJob {



    /*
     * svn地址: https://20.3.0.8/svn/%E5%BC%80%E5%8F%91%E7%AE%A1%E6%8E%A7%E7%BB%84/05%E8%83%BD%E5%8A%9B%E4%B8%AD%E5%BF%83
     */
    private String sourceRoot;

    /*
     * svn上源码所在目录
     */
    private String srcDir;

    /*
     * 本地源码目录
     */
    private String localSrcDir;

    /*
     * 源码版本, 可空
     */
    private String srcRevision;

    /**
     * 生成包类型
     * 0-zip包; 1-jar包; 2-war包
     */
    private String packType;


    /*
     * mvn打包命令,
     * 0-install(默认), 1-deploy
     */
    private String mvnCompComm;

    /*
     * source server
     */
    private String sourceServer;

    /**
     * 打包服务器IP
     */
    private String packIp;
    /**
     * 打包服务器端口
     */
    private String packPort;

    public String getPackIp() {
		return packIp;
	}

	public void setPackIp(String packIp) {
		this.packIp = packIp;
	}

	public String getPackPort() {
		return packPort;
	}

	public void setPackPort(String packPort) {
		this.packPort = packPort;
	}

	public String getSourceRoot() {
        return sourceRoot;
    }

    public void setSourceRoot(String sourceRoot) {
        this.sourceRoot = sourceRoot;
    }

    public String getSrcDir() {
        return srcDir;
    }

    public void setSrcDir(String srcDir) {
        this.srcDir = srcDir;
    }

    public String getLocalSrcDir() {
        return localSrcDir;
    }

    public void setLocalSrcDir(String localSrcDir) {
        this.localSrcDir = localSrcDir;
    }

    public String getSrcRevision() {
        return srcRevision;
    }

    public void setSrcRevision(String srcRevision) {
        this.srcRevision = srcRevision;
    }

    public String getPackType() {
        return packType;
    }

    public void setPackType(String packType) {
        this.packType = packType;
    }

    public String getMvnCompComm() {
        return mvnCompComm;
    }

    public void setMvnCompComm(String mvnCompComm) {
        this.mvnCompComm = mvnCompComm;
    }

    public String getSourceServer() {
        return sourceServer;
    }

    public void setSourceServer(String sourceServer) {
        this.sourceServer = sourceServer;
    }

}
