package com.boarsoft.boar.job;

import org.slf4j.helpers.MessageFormatter;

public class BuildJob {
	//应用源码
	public static final String SOURCE_TYPE_APP="0";
	//jar包源码
	public static final String SOURCE_TYPE_JAR="1";

	public static final String SOURCE_TYPE_FILE = "2";

	public static final String RESPCODE_SUCCESS = "success";
	public static final String RESPCODE_FAIL = "fail";

	public String respCode = RESPCODE_SUCCESS;

	public String respMsg;

	/**
	 * 回调url
	 */
	private String callBackUrl;
	/**
	 * 构建对象id
	 */
	private String targetId;
	/**
	 * 构建对象名称
	 */
	private String targetName;
	/**
	 * JenkinsBroker的构建id
	 */
	private String id;
	/**
	 * 编译、部署环境: cit/uata/uatb/pfm/preprd/prd
	 */
	private String cplEnv;
	/**
	 * 应用版本号，例: ebc-busi-acc-service-0.0.1-SNAPSHOT-bin.zip的中间部分0.0.1
	 */
	private String appVsn;
	/**
	 * 应用来源 added by OscarZhou 2017/04/17PM
	 * 0-用户主动选择
	 * 1-配置文件关联引入
	 * 2-库包关联引入
	 * 3-配置和库包共同引入
	 */
	private String appFrom;
	/**
	 * 应用构建脚本路径
	 */
	private String appShellPath;
	/**
	 * 导出构建码或编译构建码或部署构建码或停机构建码或启动/重启构建码
	 */
	private String buildCode;
	/**
	 * 应用在应用服务器上部署/编译级别 该值越大优先级越高
	 */
	private short appLevel;

	/**
	 * 源码类型：0-应用源码 1-jar包源码
	 */
	private String sourceType = SOURCE_TYPE_JAR;

	/*
	 * 源码标志: ebbc/ebcc
	 */
	private String sourceCode;

	/*
	 * tfs: http://10.16.10.97:8080/tfs/defaultcollection svn:
	 * https://20.3.0.8/svn
	 */
	private String sourceServer;

	/*
	 * tfs: $/电子银行产品线/能力中心 svn: 开发管控组/18自动化部署
	 */
	private String sourceRoot;

	/*
	 * REL_20170115/ebc-parent
	 */
	private String sourceParent;
	/*
	 * 工程目录: ebc-busi/ebc-busi-acc-service
	 */
	private String sourcePath;

	/**
	 * 编译类型
	 * 0-完整包<full>, 1-程序包<libs>, 2-配置包<conf>
	 */
	private String compType;

	/**
     * 打包服务器IP
     */
    private String packIp;
    /**
     * 打包服务器端口
     */
    private String packPort;
    
    

	public String getPackIp() {
		return packIp;
	}


	public void setPackIp(String packIp) {
		this.packIp = packIp;
	}


	public String getPackPort() {
		return packPort;
	}


	public void setPackPort(String packPort) {
		this.packPort = packPort;
	}


	public void setRespInfo(String respCode, String respMsg, Object... params) {
		this.respCode = respCode;
		this.respMsg = MessageFormatter.format(respMsg, params).getMessage();
	}


	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getSourceServer() {
		return sourceServer;
	}

	public void setSourceServer(String sourceServer) {
		this.sourceServer = sourceServer;
	}

	public String getSourceRoot() {
		return sourceRoot;
	}

	public void setSourceRoot(String sourceRoot) {
		this.sourceRoot = sourceRoot;
	}

	public String getSourceParent() {
		return sourceParent;
	}

	public void setSourceParent(String sourceParent) {
		this.sourceParent = sourceParent;
	}

	public String getCallBackUrl() {
		return callBackUrl;
	}

	public void setCallBackUrl(String callBackUrl) {
		this.callBackUrl = callBackUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCplEnv() {
		return cplEnv;
	}

	public void setCplEnv(String cplEnv) {
		this.cplEnv = cplEnv;
	}

	public String getAppVsn() {
		return appVsn;
	}

	public void setAppVsn(String appVsn) {
		this.appVsn = appVsn;
	}

	public short getAppLevel() {
		return appLevel;
	}

	public void setAppLevel(short appLevel) {
		this.appLevel = appLevel;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getBuildCode() {
		return buildCode;
	}

	public void setBuildCode(String buildCode) {
		this.buildCode = buildCode;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getCompType() {
		return compType;
	}

	public void setCompType(String compType) {
		this.compType = compType;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public String getAppFrom() {
		return appFrom;
	}

	public void setAppFrom(String appFrom) {
		this.appFrom = appFrom;
	}

	public String getAppShellPath() {
		return appShellPath;
	}

	public void setAppShellPath(String appShellPath) {
		this.appShellPath = appShellPath;
	}

	public String getRespCode() {
		return respCode;
	}

	public void setRespCode(String respCode) {
		this.respCode = respCode;
	}

	public String getRespMsg() {
		return respMsg;
	}

	public void setRespMsg(String respMsg, Object... params) {
		this.respMsg = MessageFormatter.format(respMsg, params).getMessage();
	}

}
