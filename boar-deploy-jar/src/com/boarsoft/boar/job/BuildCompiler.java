package com.boarsoft.boar.job;

import java.io.IOException;
import java.rmi.UnexpectedException;

public interface BuildCompiler {

	/**
	 * 执行编译，按部署策略将主建构拆解为若干子构建，依次执行
	 * 
	 * @param mainJob-主构建DTO
	 * @throws UnexpectedException 
	 * @throws IOException 
	 */
	void buildCompileJob(ICompileJob mainJob) throws UnexpectedException, IOException;
//	/**
//	 * 执行编译，按部署策略将主建构拆解为若干子构建，依次执行
//	 *
//	 * @param mainJob-主构建DTO
//	 * @throws UnexpectedException
//	 */
//	void buildExportJob(ExportJob mainJob) throws UnexpectedException;
}
