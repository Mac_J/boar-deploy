package com.boarsoft.boar.job.shell;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 周琳林 on 2017/3/29.
 */

import com.boarsoft.boar.job.BuildJob;

/**
 * 使用shell脚本进行文件拷贝
 */
public class ShellFileCopyJob extends BuildJob implements Serializable {
	/**
	 * 被传输/拷贝的文件分类：压缩文件
	 */
	public static final String FILE_TYPE_ZIPPED_FILE = "zippedFile";
	/**
	 * 被传输/拷贝的文件分类：jar文件
	 */
	public static final String FILE_TYPE_JAR_FILE = "jarFile";
	/**
	 * 被传输/拷贝的文件分类：text文本文件，包括各类配置文件、java文件、js文件等等单个可读的文本文件
	 */
	public static final String FILE_TYPE_TEXT_FILE = "textFile";


	/**
	 * 被传输/拷贝的文件分类
	 */
	private String fileType;
	/**
	 * 文件根路径
	 */
	private String fileSourceRootPath;
	/**
	 * 文件相对路径
	 */
	private String fileSourceRelativePath;
	/**
	 * 文件完整路径，一般为fileRootPath+fileRelativePath的结果
	 */
	private String fileSourceFullPath;
	/**
	 * 文件原始名
	 */
	private String fileSourceName;
	/**
	 * 源文件所在服务器IP
	 */
	private String fileSourceIP;
	/**
	 * 文件拷贝后的命名，针对拷贝后要重命名的情况
	 */
	private String fileDestinationName;
	/**
	 * 文件要拷贝到的服务器IP列表
	 */
	private List<String> fileDestinationIPs;
	/**
	 * 文件根路径
	 */
	private String fileDestinationRootPath;
	/**
	 * 文件相对路径
	 */
	private String fileDestinationRelativePath;
	/**
	 * 文件完整路径，一般为fileDestinationRootPath+fileDestinationRelativePath的结果
	 */
	private String fileDestinationFullPath;

	/**
	 * 应用名称，文件关联的应用名
	 */
	private String appName;

	/**
	 * 远程应用服务器用户
	 */
	private String appUsr;

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getFileSourceRootPath() {
		return fileSourceRootPath;
	}

	public void setFileSourceRootPath(String fileSourceRootPath) {
		this.fileSourceRootPath = fileSourceRootPath;
	}

	public String getFileSourceRelativePath() {
		return fileSourceRelativePath;
	}

	public void setFileSourceRelativePath(String fileSourceRelativePath) {
		this.fileSourceRelativePath = fileSourceRelativePath;
	}

	public String getFileSourceFullPath() {
		return fileSourceFullPath;
	}

	public void setFileSourceFullPath(String fileSourceFullPath) {
		this.fileSourceFullPath = fileSourceFullPath;
	}

	public String getFileSourceName() {
		return fileSourceName;
	}

	public void setFileSourceName(String fileSourceName) {
		this.fileSourceName = fileSourceName;
	}

	public String getFileSourceIP() {
		return fileSourceIP;
	}

	public void setFileSourceIP(String fileSourceIP) {
		this.fileSourceIP = fileSourceIP;
	}

	public String getFileDestinationName() {
		return fileDestinationName;
	}

	public void setFileDestinationName(String fileDestinationName) {
		this.fileDestinationName = fileDestinationName;
	}

	public List<String> getFileDestinationIPs() {
		return fileDestinationIPs;
	}

	public void setFileDestinationIPs(List<String> fileDestinationIPs) {
		this.fileDestinationIPs = fileDestinationIPs;
	}

	public String getFileDestinationRootPath() {
		return fileDestinationRootPath;
	}

	public void setFileDestinationRootPath(String fileDestinationRootPath) {
		this.fileDestinationRootPath = fileDestinationRootPath;
	}

	public String getFileDestinationRelativePath() {
		return fileDestinationRelativePath;
	}

	public void setFileDestinationRelativePath(String fileDestinationRelativePath) {
		this.fileDestinationRelativePath = fileDestinationRelativePath;
	}

	public String getFileDestinationFullPath() {
		return fileDestinationFullPath;
	}

	public void setFileDestinationFullPath(String fileDestinationFullPath) {
		this.fileDestinationFullPath = fileDestinationFullPath;
	}

	public String getAppUsr() {
		return appUsr;
	}

	public void setAppUsr(String appUsr) {
		this.appUsr = appUsr;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}


}
