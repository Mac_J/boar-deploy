package com.boarsoft.boar.job;

import java.io.IOException;
import java.rmi.UnexpectedException;

public interface BuildExporter {

	/**
	 * 执行源码导出，按部署策略将主建构拆解为若干子构建，依次执行
	 *
	 * @param mainJob-主构建DTO
	 * @throws UnexpectedException
	 * @throws IOException 
	 */
	void buildExportJob(IExportJob mainJob) throws UnexpectedException, IOException;
}
