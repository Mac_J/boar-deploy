package com.boarsoft.boar.job;

import java.io.IOException;
import java.rmi.UnexpectedException;

import com.boarsoft.boar.job.jenkins.JenkinsDeployJob;
import com.boarsoft.boar.job.shell.ShellDeploymentJob;

public interface IDeployer {
	/**
	 * 使用jenkins执行部署，按部署策略将主建构拆解为若干子构建，依次执行
	 * 
	 * @param mainJob-主构建DTO
	 * @throws UnexpectedException 
	 * @throws IOException 
	 */
	void deployByJenkins(JenkinsDeployJob mainJob) throws UnexpectedException, IOException;

	/**
	 * 通过jenkins执行停机job
	 * @throws IOException 
	 * */
	void shutDownByJenkins(BaseShutdownJob mainJob) throws UnexpectedException, IOException;

	/**
	 * 通过jenkins执行重启job
	 * @throws IOException 
	 * */
	void restartByJenkins(BaseRestartJob mainJob) throws UnexpectedException, IOException;

	/**
	 * 通过jenkins执行启动job
	 * @throws IOException 
	 * */
	void startByJenkins(BaseStartJob mainJob) throws UnexpectedException, IOException;
	/**
	 * 使用jenkins执行部署，按部署策略将主建构拆解为若干子构建，依次执行
	 *
	 * @param mainJob-主构建DTO
	 * @throws UnexpectedException
	 * @throws IOException 
	 */
	void deployByShell(ShellDeploymentJob mainJob) throws UnexpectedException, IOException;
	/**
	 * 子构建完成时的回调事件
	 * 
	 * @param id-主构建ID
	 * @param index-子构建的索引号
	 * @throws UnexpectedException 
	 * @throws IOException 
	 */
	void onJobCompleted(String id, int index, String status) throws UnexpectedException, IOException;
}
