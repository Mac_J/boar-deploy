package com.boarsoft.boar.job;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import com.boarsoft.boar.entity.ServerInfo;

public class BaseFileCopyJob extends BuildJob implements IFileCopyJob,Serializable {
	// public class JenkinsDeploymentJob extends DeploymentJob {

	private List<String> serverIdList;

	/**
	 * 当前应用ip列表
	 */
	// private ArrayList <String> appIpList;
	private List<String> appIpList;

	private String appId;

	/**
	 * 应用名称，例:
	 * ebc-busi-acc-service-0.0.1-SNAPSHOT-bin.zip的前半段ebc-busi-acc-service
	 */
	private String appName;

	/**
	 *
	 * */
	private String appSnap;

	/**
	 * 远程应用服务器用户
	 */
	private String appUsr;

	/**
	 * 应用包上传至远程应用服务器上目录
	 */
	private String appUpload;

	/**
	 * 应用服务器上部署脚本所在目录
	 */
	private String appSbin;

	/**
	 * 应用在应用服务器上部署目录
	 */
	private String appHome;
	/**
	 * 应用在应用服务器上提供服务的端口
	 */
	private String appPort;
	/**
	 * 应用在应用服务器上的临时目录
	 */
	private String tempPath;
	/**
	 * 应用或库包在服务器上的部署路径
	 * 注：1，此路径为绝对路径，2若应用或库包在多个服务器上部署，则要求他们的部署路径一致
	 */
	private String deployPath;

	/**
	 * 部署类型: 0 -- 部署单个应用包 1 -- 部署单个应用包中的bin包 2 -- 部署单个应用包中的lib包 3 -- 停单个应用 4 --
	 * 启单个应用 5 -- 重启单个应用 6 -- 部署jar包列表
	 */
	private int deployType;

	/**
	 * jar包列表
	 */
	private String appJarList;

	/**
	 * 部署策略: 0 -- 顺序部署 1 -- 半数并发 2 -- 全量并发
	 */
	private String deployPolicy;

	private String parentId;

	public String getAppUsr() {
		return appUsr;
	}

	public void setAppUsr(String appUsr) {
		this.appUsr = appUsr;
	}

	public String getAppUpload() {
		return appUpload;
	}

	public void setAppUpload(String appUpload) {
		this.appUpload = appUpload;
	}

	public String getAppSbin() {
		return appSbin;
	}

	public void setAppSbin(String appSbin) {
		this.appSbin = appSbin;
	}

	public String getAppHome() {
		return appHome;
	}

	public void setAppHome(String appHome) {
		this.appHome = appHome;
	}

	public int getDeployType() {
		return deployType;
	}

	public void setDeployType(int deployType) {
		this.deployType = deployType;
	}

	public String getAppSnap() {
		return appSnap;
	}

	public void setAppSnap(String appSnap) {
		this.appSnap = appSnap;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	// @Override
	public LinkedList<ServerInfo> getTargetList() {
		return null;
	}

	public List<String> getAppIpList() {
		return appIpList;
	}

	public void setAppIpList(List<String> appIpList) {
		this.appIpList = appIpList;
	}

	public String getDeployPolicy() {
		return deployPolicy;
	}

	public void setDeployPolicy(String deployPolicy) {
		this.deployPolicy = deployPolicy;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getAppJarList() {
		return appJarList;
	}

	public void setAppJarList(String appJarList) {
		this.appJarList = appJarList;
	}

	public String getDeployPath() {
		return deployPath;
	}

	public void setDeployPath(String deployPath) {
		this.deployPath = deployPath;
	}

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getAppPort() {
		return appPort;
	}

	public void setAppPort(String appPort) {
		this.appPort = appPort;
	}

	public List<String> getServerIdList() {
		return serverIdList;
	}

	public void setServerIdList(List<String> serverIdList) {
		this.serverIdList = serverIdList;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
}
