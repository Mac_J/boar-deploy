package com.boarsoft.boar.file.biz;

import org.springframework.stereotype.Component;

import com.boarsoft.boar.deploy.entity.FileDeployInfo;
import com.boarsoft.boar.file.FileBiz;

@Component
public class FileDeployBizImpl extends FileBizImpl implements FileBiz {
	public FileDeployBizImpl() {
		fileInfoClazz = FileDeployInfo.class;
	}
}