package com.boarsoft.boar.file.action;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.app.AppBiz;
import com.boarsoft.boar.deploy.entity.FileDeployInfo;
import com.boarsoft.boar.entity.AppInfo;
import com.boarsoft.boar.entity.ServerInfo;
import com.boarsoft.common.Authorized;
import com.boarsoft.common.Util;
import com.boarsoft.common.util.JsonUtil;

@RestController
@RequestMapping("/file")
public class FileDeployAction extends FileAction {

	@Autowired
	protected AppBiz appBiz;

	/**
	 * 在树上某个应用下增加配置文件需操作两张表 1，向file_info插入配置文件基础数据 2，向app_file插入应用与配置文件的关联关系记录
	 */
	@RequestMapping("/save.do")
	@Authorized(code = "file.save")
	public ReplyInfo<Object> save(FileDeployInfo a) {
		if (Util.strIsEmpty(a.getId())) {
			a.setStatus(ServerInfo.STATUS_RUNNING);
			a.setCreateTime(Util.getStdfDateTime());
		} else {
			a.setLastTime(Util.getStdfDateTime());
		}
		//
		AppInfo app = appBiz.get(a.getAppId());
		a.setProjId(app.getProjId());
		fileBiz.save(a.getAppId(), a);
		return new ReplyInfo<Object>(true, JsonUtil.from(a, "apps"));
	}
}