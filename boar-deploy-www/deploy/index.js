$(function(){
	App.code = 'deploy';
	App.ctx = '/' + App.code;

	App.go2 = function(to) {
		switch (to) {
		case '':
		case '#':
//			App.show({
//				ctx: 'deploy',
//				action: 'home'
//			}, $.noop, App.main);
//			break;
			App.show({
				ctx: 'base',
				action: 'dirIndex',
				params: {
					menus: [{
						title : '部署计划首页',
						ctx: 'deploy',
					}]
				}
			}, $.noop, App.main);
			break;
		case '#envIndex':
			App.show({
				ctx: 'base',
				action: 'envIndex'
			}, $.noop, App.main);
			break;
		default:
			App.show({
				ctx: 'deploy',
				action: to.substr(1)
			}, $.noop, App.main);
			break;
		}
	}
	App.go2('#');

	App.inits.push(function(){
		App.websocket.on('app', 'inst', 'alert', function(o, e, ws) {
			var ib = App.inbox, ibc = ib.children(), d = o.data;
			if (ibc.length > 3) {
				ibc.first().remove();
			}
			$('<div></div>').click(function(){
				$(this).remove();
			}).append('<span class="icon icon-alert"></span>')
				.append(d.addr + '&nbsp;异常中断').appendTo(ib);
		});
	});
});