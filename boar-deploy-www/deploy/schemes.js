App.schemes.basic.deploy = {
	faceParam: {
		width : 480,
		height : 380
	},
	faceView: {
		width : 1024,
		height : 480
	},
	comPicker: {
		width : 680,
		height : 520
	},
	planEdit: {
		width: 600,
		height: 480
	},
	planExport: {
		width: 800,
		height: 200
	},
	planFileEdit: {
		width: 600,
		height: 460
	},
	planFilePicker: {
		width: 600,
		height: 460
	},
	packgeRecord: {
		width: 1027,
		height: 450
	},
	deployRecord: {
		width: 1027,
		height: 450
	},
	appLibPicker:{
		width: 840,
		height: 500
	}
};