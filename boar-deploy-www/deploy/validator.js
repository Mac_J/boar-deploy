$(function() {
	App.deploy.validator = {
		'plan': {
			'name': {
				'empty': 0,
				'len': { min: 2, max: 64 },
				'cn_az-09s.': 0
			},
			'code': {
					'empty': 0,
					'len': { min: 2, max: 64 },
					'cn_az-09.': 0
			},
			'version': {
				'empty': 0,
				'len': { min: 1, max: 10 },
				'en_az-09.': 0
			},
			'time': {
				'empty': 0
			},
			'catalog': {
				'empty': 0
			},
			'memo': {
				'empty': 0
			},
			'packCode': {
				'empty': 0,
				'len': { min: 4, max: 64 },
				'cn_az-09': 0
			},
			'deployCode': {
				'empty': 0,
				'len': { min: 4, max: 64 },
				'cn_az-09': 0
			},
			'packAddr': {
				'empty': 0,
				'ip': 0
			}
		}
	}
});