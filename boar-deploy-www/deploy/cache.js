Cache.deploy = {
	dir: {
		types: '0,1,3,4,5,6,8,9'
	},
	pack: [ 'ZIP', 'JAR', 'WAR' ],
	logUrl : {
		head : 'http://10.16.0.161:8080/job/compile',
		headDep : 'http://10.16.0.161:8080/job/',
		foot : '/1/consoleFull'
	}
};