$(function() {
	var ctx = '/deploy';
	App.deploy = {
		'ctx': ctx,
		'home' : {
			url : ctx + '/home.htm'
		},
		'planIndex' : {
			url : ctx + '/plan/index.htm'
		},
		'planEdit' : {
			url : ctx + '/plan/edit.htm'
		},
		'planItem' : {
			url : ctx + '/plan/item.htm'
		},
		'planDetail' : {
			url : ctx + '/plan/detail.htm'
		},
		'planFileEdit' : {
			url : ctx + '/plan/file/edit.htm'
		},
		'packIndex' : {
			url : ctx + '/pack/index.htm'
		},
		'packgeRecord' : {
			url : ctx + '/pack/record.htm'
		},
		'deployIndex' : {
			url : ctx + '/deploy/index.htm'
		},
		'deployRecord' : {
			url : ctx + '/deploy/record.htm'
		},
		'appDeploy' : {
			url : ctx + '/app/deploy.htm'
		},
		'planImport' : {
			url : ctx + '/plan/import.htm'
		},
		'planExport' : {
			url : ctx + '/plan/export.htm'
		},
		'planPageList': [{
			step : 1,
			action : 'planIndex',
			ctx : 'deploy'
		}, {
			step : 2,
			backescap : true,
			title : '部署计划基本信息',
			action : 'planEdit',
			role : 'deploy.plan.get',
			openmethod : 'open',
			ctx : 'deploy'
		}, {
			step : 3,
			action : 'planItem',
			ctx : 'deploy',
			role : 'plan.item.list'
		}, {
			step : 4,
			action : 'planDetail',
			ctx : 'deploy',
			role : 'deploy.plan.detail.list'
		}, {
			step : 5,
			action : 'packIndex',
			ctx : 'deploy',
			role : 'deploy.plan.pack.list'
		}, {
			step : 6,
			action : 'deployIndex',
			ctx : 'deploy',
			role : 'deploy.plan.deploy.list'
		}]
	}
});
