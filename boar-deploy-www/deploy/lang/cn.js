Msg.build = {
	info : '构建信息'	
};
Msg.pack = {
	record : '包记录',
	param : '包参数',
	'package' : '打包'
};
Msg.deploy = {
	all : '全部部署',
	one : '单个部署',
	inst : '服务器实例部署'
};
Msg.error = {
	noResponse : "Server no response.",
	unknown : "{0} 失败, 未知异常",
	inUse : "{0} failed! {1} has already been used.",
	needParam : "{0} failed! Missing parameter: {1}.",
	invalid: "{0} 不合法",
	notFound: "{0} 不存在",
	unauth: '未授权',
	duplicated: "{0}重复",
	expMetaGen : "元数据文件生成失败，请联系管理员。",
	expMetaCp : "元数据文件拷贝失败，请联系管理员。",
	expFileCp : "部署文件拷贝失败，请确认部署文件是否存在。"
};
