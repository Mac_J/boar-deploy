package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

/**
 * 此类是BuildPlanItem与 Server之间的多对多关系实体<br>
 * 一个BuildPlanItem对应多个BuildPlanDetail
 */
public class BuildPlanDetail implements Serializable {
	private static final long serialVersionUID = 677579284945352164L;

	public static final short STATUS_NOUSE = 0;
	public static final short STATUS_INUSE = 1;

	/** ID */
	private String id;
	/** 计划id */
	private String planId;
	/** appId/libId */
	private String targetId;
	/** 目标在服务器上的临时路径 */
	private String tempPath;
	/** 目标在服务器上的部署路径 */
	private String targetPath;
	/** 目标在服务器上的部署端口 */
	private int targetPort;
	/** 计划要部署的服务器id */
	private String serverId;
	/** 冗余目标服务器code */
	private String code;
	/** 冗余目标服务器name */
	private String name;
	/** 冗余目标服务器ip */
	private String ip;
	/** 冗余目标服务器上的用户 */
	private String user;
	/** 冗余目标服务器所属env */
	private String env;
	/** 冗余目标服务器group */
	private String group;
	/** 部署状态 */
	private short status;

	public String getAddr() {
		return new StringBuilder().append(ip)//
				.append(":").append(targetPort).toString();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getTempPath() {
		return tempPath;
	}

	public void setTempPath(String tempPath) {
		this.tempPath = tempPath;
	}

	public String getTargetPath() {
		return targetPath;
	}

	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}

	public int getTargetPort() {
		return targetPort;
	}

	public void setTargetPort(int targetPort) {
		this.targetPort = targetPort;
	}

	public short getStatus() {
		return status;
	}

	public void setStatus(short status) {
		this.status = status;
	}
}