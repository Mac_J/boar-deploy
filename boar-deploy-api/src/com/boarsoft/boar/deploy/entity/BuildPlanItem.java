package com.boarsoft.boar.deploy.entity;

import java.io.Serializable;

import com.boarsoft.bean.User;

public class BuildPlanItem implements Serializable {
	private static final long serialVersionUID = 5364530758162777312L;

	/** ITEM TYPE 0：应用，1：库包，2：配置文件 */
	public static final short TYPE_APP = 0;
	/** ITEM TYPE 0：应用，1：库包，2：配置文件 */
	public static final short TYPE_LIB = 1;
	/** ITEM TYPE 0：应用，1：库包，2：配置文件 */
	public static final short TYPE_CONF = 2;
	/** ITEM TYPE 0：应用，1：库包，2：配置文件 */
	public static final short TYPE_SQL = 3;
	/** ITEM TYPE 0：应用，1：库包，2：配置文件 */
	public static final short TYPE_SHELL = 4;

	/** ITEM ID */
	private String id;
	/** Plan ID */
	private String planId;
	/** ITEM CODE */
	private String code;
	/** 应用名称 */
	private String name;
	/** ITEM版本 */
	private String ver;
	/** 应用id或库包id或配置文件id */
	private String targetId;
	/** ITEM TYPE 0：应用，1：库包，2：配置文件 */
	private short type;
	/** 完整的源码URL */
	private String sourcePath;
	/** 源码分支 */
	private String branch;
	/** 完整的打包目录路径 */
	private String packPath;
	/** 完整的打包目录路径 */
	private String remotePath;
	/** 备注 */
	private String memo = "";
	/** 添加者，为空时表示是自动带入的 **/
	private String ownerId;
	/** Item 创建时间 */
	private String createTime;
	/** Item最后更新时间 */
	private String lastTime;

	/** 创建人（不映射） */
	private User owner;

	@Override
	public String toString() {
		return id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public short getType() {
		return type;
	}

	public void setType(short type) {
		this.type = type;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getLastTime() {
		return lastTime;
	}

	public void setLastTime(String lastTime) {
		this.lastTime = lastTime;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getSourcePath() {
		return sourcePath;
	}

	public void setSourcePath(String sourcePath) {
		this.sourcePath = sourcePath;
	}

	public String getBranch() {
		if(branch==null){
			branch="master";
		}
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getPackPath() {
		return packPath;
	}

	public void setPackPath(String packPath) {
		this.packPath = packPath;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public User getOwner() {
		return owner;
	}

	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}
}