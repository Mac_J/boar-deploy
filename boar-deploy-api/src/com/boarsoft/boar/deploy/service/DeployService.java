package com.boarsoft.boar.deploy.service;

import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.boar.deploy.plan.bean.BuildRequest;

/**
 * 由自动化部署提供的部署服务
 * 
 * @author Mac_J
 *
 */
public interface DeployService {

	ReplyInfo<Object> execute(String id, String env);

	ReplyInfo<Object> refresh(String id, String env);

	ReplyInfo<Object> execute(BuildRequest p);
}