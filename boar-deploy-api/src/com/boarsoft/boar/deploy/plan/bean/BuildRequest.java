package com.boarsoft.boar.deploy.plan.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.boarsoft.boar.deploy.entity.BuildPlanItem;

public class BuildRequest implements Serializable {
	private static final long serialVersionUID = -1000054825510857152L;

	/** 部署计划ID */
	protected String planId;
	/** */
	protected String env;
	/** websocket session id */
	protected String wssid;
	/** user token */
	protected String token;
	/** 相对于用户的本次请求的ID */
	protected String id;
	/** 当前将部署的appId */
	protected String currentAppId;
	/** 当前将部署的计划 */
	protected BuildPlanItem curPlanItem;
	/** */
	protected List<BuildPlanItem> items = new ArrayList<>();
	/** 其它参数 */
	protected Map<String, Object> data = new ConcurrentHashMap<String, Object>();

	public void put(String key, Object value) {
		data.put(key, value);
	}

	public Object get(String key) {
		return data.get(key);
	}

	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> c, String key) {
		return (T) data.get(key);
	}

	public boolean exists(String key) {
		return data.containsKey(key);
	}

	public void addItem(BuildPlanItem o) {
		items.add(o);
	}

	public String getWssid() {
		return wssid;
	}

	public void setWssid(String wssid) {
		this.wssid = wssid;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<BuildPlanItem> getItems() {
		return items;
	}

	public void setItems(List<BuildPlanItem> items) {
		this.items = items;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

	public String getCurrentAppId() {
		return currentAppId;
	}

	public void setCurrentAppId(String currentAppId) {
		this.currentAppId = currentAppId;
	}

	public BuildPlanItem getByAppId(String appId) {
		for(BuildPlanItem buildPlanItem:items){
			if(buildPlanItem.getTargetId().equals(appId)){
				return buildPlanItem;
			}
		}
		return null;
	}

	public BuildPlanItem getCurPlanItem() {
		return curPlanItem;
	}

	public void setCurPlanItem(BuildPlanItem curPlanItem) {
		this.curPlanItem = curPlanItem;
	}
}