package com.boarsoft.boar.agent;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.boarsoft.agent.AgentService;
import com.boarsoft.bean.ReplyInfo;
import com.boarsoft.rpc.core.RpcContext;

public class Main {
	private static Logger log = LoggerFactory.getLogger(Main.class);
	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) throws Exception {
		ctx = new ClassPathXmlApplicationContext("classpath:conf/context.xml");
		System.out.println("Startup ".concat(ctx.isRunning() ? "successfully." : "failed."));
		
		for (int i=0;i<10000;i++) {
			String s = "000000000002";
			Integer.parseInt(s);
			Integer.parseInt("000000000002");
		}
		System.out.println("000000000000000000000000000000000000");

		// testSql(ctx);
		// for (int i = 0; i < 1000000; i++) {
		// testShell(ctx);
		// }
		// AgentServiceImpl as = new AgentServiceImpl();
		// ReplyInfo<String> ri = as.executeShell("/home/web/temp", "exec.sh",
		// new String[] { "/home/web/temp/echo.sh", "\"a=1&b=2\"" });
		// System.out.println(JsonUtil.from(ri));

		testEcho(ctx);
		// testShell(ctx);
	}

	public static void testEcho(ApplicationContext ctx) {
		AgentService as = (AgentService) ctx.getBean("agentX");
		// RpcContext.specify2("10.16.0.164:9901");
		try {
			ReplyInfo<Object> ri = as.executeShell(new String[] { "echo", "hello!" });
			System.out.println(ri.getData());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			// RpcContext.specify2(null);
		}
	}

	public static void testShell(ApplicationContext ctx) throws Throwable {
		AgentService as = (AgentService) ctx.getBean("agentX");
		ReplyInfo<String> ri = null;

		RpcContext.specify2("10.16.0.161:9901");
		try {
//			ri = as.executeShell2("web", "10.16.0.164",
//					"sh /home/web/boar-deploy-agent/sbin/exec.sh /home/web/boar-deploy-agent/sbin/ restart.sh");
			System.out.println(ri.getData());
		} finally {
			RpcContext.specify2(null);
		}

		// RpcContext.specify2("10.16.0.161:9901");
		// try {
		// // ri = as.executeShell("/shell/copy.bat", new String[] {
		// // "E:\\temp\\sql", "20.3.5.252", "\\temp\\20170315\\sql" });
		// ri = as.executeShell(new String[] { "sh",
		// "/home/nlzx/boar-deploy-agent//shell/rcp.sh",
		// "/home/nlzx/boar-web/src/CIT/boar-svn/boar-base-jar/target",
		// "boar-base-jar-1.0.0.jar", "0", "10.16.0.164",
		// "/opt/tomcat8.5.9/webapps/boar/./WEB-INF/lib",
		// "boar-base-jar-1.0.0.jar", "web" });
		// System.out.println(ri.getData());
		// } finally {
		// RpcContext.specify2(null);
		// }
		// RpcContext.specify2("10.16.0.161:9901");
		// try {
		// ri = as.executeCmd("rcp", new String[] {
		// "/home/nlzx/boar-web/src/CIT/boar-svn/boar-base-jar/target",
		// "boar-base-jar-1.0.0.jar", "0", "10.16.0.164",
		// "/opt/tomcat8.5.9/webapps/boar/./WEB-INF/lib",
		// "boar-base-jar-1.0.0.jar", "web" });
		// System.out.println(ri.getData());
		// } finally {
		// RpcContext.specify2(null);
		// }
		// as.executeShell(
		// "xcopy E:/temp/sql/001.sql \\\\20.3.5.252\\temp\\sql\\002.sql");
		if (!ri.isSuccess()) {
			System.out.println(ri.getData());
			return;
		}
	}

	public static void testSql(ApplicationContext ctx) throws Throwable {
		AgentService as = (AgentService) ctx.getBean("agentX");
		ReplyInfo<String> ri = null;
		/*
		 * // RpcContext.specify2("20.3.5.252:9903");
		 * RpcContext.specify2("10.16.40.226:9902"); try { ri =
		 * as.executeShell("/shell/copy.bat", new String[] { "E:\\temp\\sql",
		 * "20.3.5.252", "\\temp\\20170315\\sql" }); } finally {
		 * RpcContext.specify2(null); } // as.executeShell( //
		 * "xcopy E:/temp/sql/001.sql \\\\20.3.5.252\\temp\\sql\\002.sql"); if
		 * (!ri.isSuccess()) { System.out.println(ri.getData()); return; }
		 */
		// RpcContext.specify2("20.3.5.252:9903");
		log.info("Call executeSql on 10.16.40.226:9902");
		RpcContext.specify2("10.16.40.226:9902");
		try {
//			ri = as.executeSql("boar", "/001.sql");
		} finally {
			RpcContext.specify2(null);
		}
		log.info("executeSql return from 10.16.40.226:9902");
		if (!ri.isSuccess()) {
			System.out.println(ri.getData());
			return;
		}
	}
}
